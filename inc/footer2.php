<footer>
	<div class="wrapper">
		<div class="contact-footer">
			<address>
				<span><?=$nomeSite." - ".$slogan?></span>
			</address>
		</div>
		<div class="menu-footer">
			<nav>
				<ul>
					<li><a href="<?=$url?>" title="Página inicial">Home</a></li>
					<li><a href="<?=$url?>produtos" title="Produtos">Produtos</a></li>
					<li><a href="<?=$url?>informacoes" title="Informacoes">Informações</a></li>
					<li><a href="<?=$url?>sobre-nos" title="Sobre nós">Sobre nós</a></li>
					<li><a href="<?=$url?>mapa-site" title="Mapa do site <?=$nomeSite?>">Mapa do site</a></li>
          <li><a href="https://faca-parte.solucoesindustriais.com.br/" title="Faça parte" target="_blank" class="faca-parte"> Faça parte</a></li>
        </ul>
			</nav>
		</div>
		<br class="clear">
	</div>
</footer>
<div class="copyright-footer">
	<div class="wrapper-footer">
		Copyright © <?=$nomeSite?>. (Lei 9610 de 19/02/1998)
		<div class="center-footer">
			<img style="width: 30%;" src="imagens/img-home/logo-footer.png" alt="<?=$nomeSite?>" title="<?=$nomeSite?>">
			<p class="footer-p">é um parceiro</p>
			<img src="imagens/logo-solucs.png" alt="Soluções Industriais" title="Soluções Industriais">
		</div>
		<div class="selos">
			<a rel="noopener nofollow" href="http://validator.w3.org/check?uri=<?=$url.$urlPagina?>" target="_blank" title="HTML5 W3C"><i class="fab fa-html5"></i> <strong>W3C</strong></a>
			<a rel="noopener nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?=$url.$urlPagina?>" target="_blank" title="CSS W3C" ><i class="fab fa-css3"></i> <strong>W3C</strong></a>
		</div>
	</div>
</div>

<script async src="<?=$url?>js/jquery.scrollUp.min.js"></script> 
<script async src="<?=$url?>js/scroll.js"></script>


<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-D23WW3S4NC');
</script>
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-JRVD3L6JXY"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-JRVD3L6JXY');
</script>

<!-- /BOTAO SCROLL -->
<script async src="<?=$url?>js/vendor/modernizr-2.6.2.min.js"></script>
<script async src="<?=$url?>js/app.js"></script>

<script defer src="<?=$url?>js/jquery.slicknav.js"></script>
<script><? include("js/modernizr-2.6.2.min.js");?></script>
<?php  ?>

<!-- Shark Orcamento -->
<div id="sharkOrcamento" style="display: none;"></div>
<script>
	var guardar = document.querySelectorAll('.botao-cotar');
		for(var i = 0; i < guardar.length; i++){
		guardar[i].removeAttribute('href');
  var adicionando = guardar[i].parentNode;
	adicionando.classList.add('nova-api');
};
</script>


<!-- Google Analytics -->

<!-- window.performance -->
<script>
  var myTime = window.performance.now();
  var items = window.performance.getEntriesByType('mark');
  var items = window.performance.getEntriesByType('measure');
  var items = window.performance.getEntriesByName('mark_fully_loaded');
  window.performance.mark('mark_fully_loaded');
  window.performance.measure('measure_load_from_dom', 'mark_fully_loaded');
  window.performance.clearMarks();
  window.performance.clearMeasures('measure_load_from_dom');
</script>
<script>
// Função para pegar o value do primeiro h2 da página
function getFirstH2Value() {
  const h2 = document.querySelector("h2");
  return h2.textContent;
}

// Função para pegar os primeiros 200 caracteres do primeiro <p> após o primeiro h2
function getFirstPValue() {
  const p = document.querySelector("h2 + p");
  const text = p.textContent.slice(0, 200);
  return text + "...";
}

// Criação do objeto de schema inicial
const schema = {
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [
    {
      "@type": "Question",
      "name": getFirstH2Value(),
      "acceptedAnswer": {
        "@type": "Answer",
        "text": getFirstPValue()
      }
    }
  ]
};

// Função para adicionar uma nova pergunta e resposta ao schema inicial
function addNewQuestion() {
  const secondH2 = document.querySelector("h2:nth-of-type(3)");
  const secondP = document.querySelector("h2:nth-of-type(3) + p");
  if (secondH2 && secondP) {
    schema.mainEntity.push({
      "@type": "Question",
      "name": secondH2.textContent,
      "acceptedAnswer": {
        "@type": "Answer",
        "text": secondP.textContent
      }
    });
  }
}

// Chama a função para adicionar uma nova pergunta e resposta, caso exista
addNewQuestion();

// Criação do elemento script no footer com o valor do schema criado
const script = document.createElement("script");
script.type = "application/ld+json";
script.text = JSON.stringify(schema);
document.querySelector("footer").appendChild(script);

</script>


<!-- Shark Orcamento -->
<div id="sharkOrcamento" style="display: none;"></div>
<script>
    var guardar = document.querySelectorAll('.botao-cotar');
        for(var i = 0; i < guardar.length; i++){
        guardar[i].removeAttribute('href');
  var adicionando = guardar[i].parentNode;
    adicionando.classList.add('nova-api');
};
</script>


<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script>
