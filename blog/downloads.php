<?php
if (!$Read):
  $Read = new Read;
endif;
$arrBreadcrump = array();
if (isset($URL) && !in_array('', $URL)):
  $lastCategory = end($URL);
foreach ($URL as $paginas => $value):
  if (!empty($value)):
    $Read->ExeRead(TB_CATEGORIA, "WHERE cat_name = :nm AND cat_status = :st AND user_empresa = :emp", "nm={$value}&st=2&emp=" . EMPRESA_CLIENTE);
    if ($Read->getResult()):
      $itemSessao = $Read->getResult();
      $arrBreadcrump[] = array('titulo' => $itemSessao[0]['cat_title'], 'url' => $itemSessao[0]['cat_name'], 'parent' => $itemSessao[0]['cat_id']);
    endif;
    $Read->ExeRead(TB_DOWNLOAD, "WHERE dow_name = :nm AND user_empresa = :emp", "nm={$value}&emp=" . EMPRESA_CLIENTE);
    if ($Read->getResult()):
      $itemName = $Read->getResult();
      $arrBreadcrump[] = array('titulo' => $itemName[0]['dow_title'], 'url' => $itemName[0]['dow_name'], 'parent' => $itemName[0]['cat_parent']);
    endif;
  endif;
endforeach;
endif;
include('inc/head.php');
 ?>
<style><?include ('slick/slick.css');?></style>
</head>
<body>
  <?php include('inc/topo.php'); ?>
  <main>
    <div id="downloads">
      <section>
        <div class="wrapper">
          <?php Check::SetBreadcrumb($arrBreadcrump); ?>
          <h1 class="breadcrumb-sig__title"><?php Check::SetTitulo($arrBreadcrump, $URL); ?> </h1>
          <?php
          $categ = Check::CatByName($lastCategory, EMPRESA_CLIENTE);
          $capas = new Read;
          $capas->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_status = :stats AND cat_id = :id ORDER BY cat_date DESC", "emp=" . EMPRESA_CLIENTE . "&stats=2&id={$categ}");
          $capas = $capas->getResult();
          $capas = $capas[0];
          $Read->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_status = :stats AND cat_parent = :parent ORDER BY cat_date DESC", "emp=" . EMPRESA_CLIENTE . "&stats=2&parent={$categ}");
          if (!$Read->getResult()):
            $Read->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_status = :stats AND cat_id = :parent ORDER BY cat_date DESC", "emp=" . EMPRESA_CLIENTE . "&stats=2&parent={$categ}");
            if ($Read->getResult()):
              if (!empty($cat_content)): ?>
                <div class="container">
                  <div class="category-content">
                    <?= $capas['cat_content']; ?>
                  </div>
                </div>
              <? endif;
            endif; ?>
            <div class="container">
              <div class="grid grid-col-2">
                <?php
                $Read->ExeRead(TB_DOWNLOAD, "WHERE user_empresa = :emp AND cat_parent = :cat AND dow_status = :stats ORDER BY dow_date DESC", "emp=" . EMPRESA_CLIENTE . "&cat={$categ}&stats=2");
                if ($Read->getResult()):
                  foreach ($Read->getResult() as $dow):
                    extract($dow); ?>
                    <div class="download-card" id="<?= $dow_name; ?>">
                      <a class="download-card__link" href="<?= BASE . '/uploads/' . $dow_file; ?>" title="<?= $dow_title; ?>" target="_blank">
                        <i class="download-card__icon fas fa-file-pdf"></i>
                        <div class="download-card__description">
                          <h2 class="download-card__title"><?= $dow_title; ?></h2>
                          <p class="download-card__text"><?= Check::Words($dow_description, 10); ?></p>
                        </div>
                      </a>
                    </div>
                  <? endforeach;
                endif; ?>
              </div>
            </div>
          <?php else:
            if (!empty($cat_content)): ?>
              <div class="container">
                <div class="category-content">
                  <?= $capas['cat_content']; ?>
                </div>
              </div>
            <? endif; ?>
            <div class="container-download">
              <?php
              foreach ($Read->getResult() as $load):
                extract($load); ?>
                <div class="download-card">
                  <a class="download-card__link" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $cat_name; ?>" title="<?= $cat_title; ?>">
                    <i class="download-card__icon fas fa-pdf"></i>
                    <div class="download-card__description">
                      <h2 class="download-card__title"><?= $cat_title; ?></h2>
                      <p class="download-card__text"><?= Check::Words($cat_description, 10); ?></p>
                    </div>
                  </a>
                </div>
              <?php endforeach;
              $Read->ExeRead(TB_DOWNLOAD, "WHERE cat_parent = :cat AND user_empresa = :emp", "cat={$categ}&emp=" . EMPRESA_CLIENTE);
              if ($Read->getResult()):
                foreach ($Read->getResult() as $download):
                  extract($download); ?>
                  <div class="download-card" id="<?= $dow_name; ?>">
                    <a href="<?= BASE . '/uploads/' . $dow_file; ?>" title="<?= $dow_title; ?>" target="_blank">
                      <i class="download-card__icon fas fa-file-pdf"></i>
                      <div class="download-card__description">
                        <h2 class="download-card__title"><?= $dow_title; ?></h2>
                        <p class="download-card__text"><?= Check::Words($dow_description, 10); ?></p>
                      </div>
                    </a>
                  </div>
                <? endforeach;
              endif; ?>
            </div>
          <? endif; ?>
        </div> <!-- wrapper -->
      </section>
    </div> <!-- downloads -->
  </main>
  <?php include('inc/footer.php'); ?>
</body>
</html>