
<h2>Regiões onde a <?=$nomeSite?> atende <?=$h1?>:</h2>
<div id="servicosTabs">
      <div>
            <ul class="nav">
                  <li class="nav-two"><a href="#selecione2" class="current" title="Selecione">Selecione</a></li>
                  <li class="nav-two"><a href="#rj" title="RJ - Rio de Janeiro">RJ</a></li>
                  <li class="nav-two"><a href="#mg" title="MG - Minas Gerais">MG</a></li>
                  <li class="nav-two"><a href="#es" title="ES - Espírito Santo">ES</a></li>
                  <li class="nav-two"><a href="#sp" title="SP - Litoral e Interior">SP</a></li>
                  <li class="nav-two"><a href="#pr" title="PR - Paraná">PR</a></li>
                  <li class="nav-two"><a href="#sc" title="SC - Santa Catarina">SC</a></li>
                  <li class="nav-two"><a href="#rs" title="RS - Rio Grande do Sul">RS</a></li>
                  <li class="nav-two"><a href="#pe" title="PE - Pernambuco">PE</a></li>
                  <li class="nav-two"><a href="#ba" title="BA - Bahia">BA</a></li>
                  <li class="nav-two"><a href="#ce" title="CE - Ceará">CE</a></li>
                  <li class="nav-two"><a href="#go" title="GO e DF - Goiás e Distrito Federal">GO e DF</a></li>
                  <li class="nav-two"><a href="#am" title="AM - Amazonas">AM</a></li>
                  <li class="nav-two"><a href="#pa" title="PA - Pará">PA</a></li>
            </ul>
      </div>
      <div class="list-wrap">
            <ul id="selecione2">
                  <li>Selecione a região do Brasil</li>
            </ul>
            <ul id="rj" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Rio de Janeiro</li>
                  <li>São Gonçalo</li>
                  <li>Duque de Caxias</li>
                  <li>Nova Iguaçu</li>
                  <li>Niterói</li>
                  <li>Belford Roxo</li>
                  <li>São João de Meriti</li>
                  <li>Campos dos Goytacazes</li>
                  <li>Petrópolis</li>
                  <li>Volta Redonda</li>
                  <li>Magé</li>
                  <li>Itaboraí</li>
                  <li>Mesquita</li>
                  <li>Nova Friburgo</li>
                  <li>Barra Mansa</li>
                  <li>Macaé</li>
                  <li>Cabo Frio</li>
                  <li>Nilópolis</li>
                  <li>Teresópolis</li>
                  <li>Resende</li>
            </ul>
            <ul id="mg" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Belo Horizonte</li>
                  <li>Uberlândia</li>
                  <li>Contagem</li>
                  <li>Juiz de Fora</li>
                  <li>Betim</li>
                  <li>Montes Claros</li>
                  <li>Ribeirão das Neves</li>
                  <li>Uberaba</li>
                  <li>Governador Valadares</li>
                  <li>Ipatinga</li>
                  <li>Santa Luzia</li>
                  <li>Sete Lagoas</li>
                  <li>Divinópolis</li>
                  <li>Ibirité</li>
                  <li>Poços de Caldas</li>
                  <li>Patos de Minas</li>
                  <li>Teófilo Otoni</li>
                  <li>Sabará</li>
                  <li>Pouso Alegre</li>
                  <li>Barbacena</li>
                  <li>Varginha</li>
                  <li>Conselheiro Lafeiete</li>
                  <li>Araguari</li>
                  <li>Itabira</li>
                  <li>Passos</li>
            </ul>
            <ul id="es" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Serra</li>
                  <li>Vila Velha</li>
                  <li>Cariacica</li>
                  <li>Vitória</li>
                  <li>Cachoeiro de Itapemirim</li>
                  <li>Linhares</li>
                  <li>São Mateus</li>
                  <li>Colatina</li>
                  <li>Guarapari</li>
                  <li>Aracruz</li>
                  <li>Viana</li>
                  <li>Nova Venécia</li>
                  <li>Barra de São Francisco</li>
                  <li>Santa Maria de Jetibá</li>
                  <li>Castelo</li>
                  <li>Marataízes</li>
                  <li>São Gabriel da Palha</li>
                  <li>Domingos Martins</li>
                  <li>Itapemirim</li>
                  <li>Afonso Cláudio</li>
                  <li>Alegre</li>
                  <li>Baixo Guandu</li>
                  <li>Conceição da Barra</li>
                  <li>Guaçuí</li>
                  <li>Iúna</li>
                  <li>Jaguaré</li>
                  <li>Mimoso do Sul</li>
                  <li>Sooretama</li>
                  <li>Anchieta</li>
                  <li>Pinheiros</li>
                  <li>Pedro Canário</li>
            </ul>
            <ul id="sp" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Bertioga</li>
                  <li>Caraguatatuba</li>
                  <li>Cubatão</li>
                  <li>Guarujá</li>
                  <li>Ilhabela</li>
                  <li>Itanhaém</li>
                  <li>Mongaguá</li>
                  <li>Riviera de São Lourenço</li>
                  <li>Santos</li>
                  <li>São Vicente</li>
                  <li>Praia Grande</li>
                  <li>Ubatuba</li>
                  <li>São Sebastião</li>
                  <li>Peruíbe</li>
                  <li>São José dos campos</li>
                  <li>Campinas</li>
                  <li>Jundiaí</li>
                  <li>Sorocaba</li>
                  <li>Indaiatuba&nbsp;</li>
                  <li>São José do Rio Preto&nbsp;</li>
                  <li>Itatiba&nbsp;</li>
                  <li>Amparo&nbsp;</li>
                  <li>Barueri&nbsp;</li>
                  <li>Ribeirão Preto</li>
                  <li>Marília&nbsp;</li>
                  <li>Louveira&nbsp;</li>
                  <li>Paulínia&nbsp;</li>
                  <li>Bauru&nbsp;</li>
                  <li>Valinhos&nbsp;</li>
                  <li>Bragança Paulista&nbsp;</li>
                  <li>Araraquara</li>
                  <li>Americana</li>
                  <li>Atibaia&nbsp;</li>
                  <li>Taubaté&nbsp;</li>
                  <li>Araras&nbsp;</li>
                  <li>São Carlos&nbsp;</li>
                  <li>Itupeva&nbsp;</li>
                  <li>Mendonça&nbsp;</li>
                  <li>Itu&nbsp;</li>
                  <li>Vinhedo&nbsp;</li>
                  <li>Marapoama&nbsp;</li>
                  <li>Votuporanga&nbsp;</li>
                  <li>Hortolândia&nbsp;</li>
                  <li>Araçatuba&nbsp;</li>
                  <li>Jaboticabal&nbsp;</li>
                  <li>Sertãozinho</li>
            </ul>
            <ul id="pr" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Curitiba</li>
                  <li>Londrina</li>
                  <li>Maringá</li>
                  <li>Ponta Grossa</li>
                  <li>Cascavel</li>
                  <li>São José dos Pinhais</li>
                  <li>Foz do Iguaçu</li>
                  <li>Colombo</li>
                  <li>Guarapuava</li>
                  <li>Paranaguá</li>
                  <li>Araucária</li>
                  <li>Toledo</li>
                  <li>Apucarana</li>
                  <li>Pinhais</li>
                  <li>Campo Largo</li>
                  <li>Almirante Tamandaré</li>
                  <li>Umuarama</li>
                  <li>Paranavaí</li>
                  <li>Piraquara</li>
                  <li>Cambé</li>
                  <li>Sarandi</li>
                  <li>Fazenda Rio Grande</li>
                  <li>Paranavaí</li>
                  <li>Francisco Beltrão</li>
                  <li>Pato Branco</li>
                  <li>Cianorte</li>
                  <li>Telêmaco Borba</li>
                  <li>Castro</li>
                  <li>Rolândia</li>
            </ul>
            <ul id="sc" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Joinville</li>
                  <li>Florianópolis</li>
                  <li>Blumenau</li>
                  <li>Itajaí</li>
                  <li>São José</li>
                  <li>Chapecó</li>
                  <li>Criciúma</li>
                  <li>Jaraguá do sul</li>
                  <li>Lages</li>
                  <li>Palhoça</li>
                  <li>Balneário Camboriú</li>
                  <li>Brusque</li>
                  <li>Tubarão</li>
                  <li>São Bento do Sul</li>
                  <li>Caçador</li>
                  <li>Concórdia</li>
                  <li>Camboriú</li>
                  <li>Navegantes</li>
                  <li>Rio do Sul</li>
                  <li>Araranguá</li>
                  <li>Gaspar</li>
                  <li>Biguaçu</li>
                  <li>Indaial</li>
                  <li>Mafra</li>
                  <li>Canoinhas</li>
                  <li>Itapema</li>
            </ul>
            <ul id="rs" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Porto Alegre</li>
                  <li>Caxias do Sul</li>
                  <li>Pelotas</li>
                  <li>Canoas</li>
                  <li>Santa Maria</li>
                  <li>Gravataí</li>
                  <li>Viamão</li>
                  <li>Novo Hamburgo</li>
                  <li>São Leopoldo</li>
                  <li>Rio Grande</li>
                  <li>Alvorada</li>
                  <li>Passo Fundo</li>
                  <li>Sapucaia do Sul</li>
                  <li>Uruguaiana</li>
                  <li>Santa Cruz do Sul</li>
                  <li>Cachoeirinha</li>
                  <li>Bagé</li>
                  <li>Bento Gonçalves</li>
                  <li>Erechim</li>
                  <li>Guaíba</li>
                  <li>Cachoeira do Sul</li>
                  <li>Santana do Livramento</li>
                  <li>Esteio</li>
                  <li>Ijuí</li>
                  <li>Alegrete</li>
            </ul>
            <ul id="pe" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Recife</li>
                  <li>Jaboatão dos Guararapes</li>
                  <li>Olinda</li>
                  <li>Bandeira caruaru.jpg&nbsp;Caruaru</li>
                  <li>Petrolina</li>
                  <li>Paulista</li>
                  <li>Cabo de Santo Agostinho</li>
                  <li>Camaragibe</li>
                  <li>Garanhuns</li>
                  <li>Vitória de Santo Antão</li>
                  <li>Igarassu</li>
                  <li>São Lourenço da Mata</li>
                  <li>Abreu e Lima</li>
                  <li>Santa Cruz do Capibaribe</li>
                  <li>Ipojuca</li>
                  <li>Serra Talhada</li>
                  <li>Araripina</li>
                  <li>Gravatá</li>
                  <li>Carpina</li>
                  <li>Goiana</li>
                  <li>Belo Jardim</li>
                  <li>Arcoverde</li>
                  <li>Ouricuri</li>
                  <li>Escada</li>
                  <li>Pesqueira</li>
                  <li>Surubim</li>
                  <li>Palmares</li>
                  <li>Bezerros</li>
            </ul>
            <ul id="ba" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Salvador</li>
                  <li>Feira de Santana</li>
                  <li>Vitória da Conquista</li>
                  <li>Camaçari</li>
                  <li>Itabuna</li>
                  <li>Juazeiro</li>
                  <li>Lauro de Freitas</li>
                  <li>Ilhéus</li>
                  <li>Jequié</li>
                  <li>Teixeira de Freitas</li>
                  <li>Alagoinhas</li>
                  <li>Barreiras</li>
                  <li>Porto Seguro</li>
                  <li>Simões Filho</li>
                  <li>Paulo Afonso</li>
                  <li>Eunápolis</li>
                  <li>Santo Antônio de Jesus</li>
                  <li>Valença</li>
                  <li>Candeias</li>
                  <li>Guanambi</li>
                  <li>Jacobina</li>
                  <li>Serrinha</li>
                  <li>Senhor do Bonfim</li>
                  <li>Dias d'Ávila</li>
                  <li>Luís Eduardo Magalhães</li>
                  <li>Itapetinga</li>
                  <li>Irecê</li>
                  <li>Campo Formoso</li>
                  <li>Casa Nova</li>
                  <li>Brumado</li>
                  <li>Bom Jesus da Lapa</li>
                  <li>Conceição do Coité</li>
                  <li>Itamaraju</li>
                  <li>Itaberaba</li>
                  <li>Cruz das Almas</li>
                  <li>Ipirá</li>
                  <li>Santo Amaro</li>
                  <li>Euclides da Cunha</li>
            </ul>
            <ul id="ce" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Fortaleza</li>
                  <li>caucacia</li>
                  <li>Juazeiro do Norte</li>
                  <li>Maracanaú</li>
                  <li>Sobral</li>
                  <li>Crato</li>
                  <li>Itapipoca</li>
                  <li>Maranguape</li>
                  <li>Iguatu</li>
                  <li>Quixadá</li>
                  <li>Canindé</li>
                  <li>Pacajus</li>
                  <li>Crateús</li>
                  <li>Aquiraz</li>
                  <li>Pacatuba</li>
                  <li>Quixeramobim</li>
            </ul>
            <ul id="ma" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>São Luís</li>
                  <li>Imperatriz</li>
                  <li>São José de Ribamar</li>
                  <li>Timon</li>
                  <li>Caxias</li>
                  <li>Codó</li>
                  <li>Paço do Lumiar</li>
                  <li>Açailândia</li>
                  <li>Bacabal</li>
                  <li>Balsas</li>
                  <li>Barra do Corda</li>
            </ul>
            <ul id="pi" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Teresina</li>
                  <li>São Raimundo Nonato</li>
                  <li>Parnaíba</li>
                  <li>Picos</li>
                  <li>Uruçuí</li>
                  <li>Floriano</li>
                  <li>Piripiri</li>
                  <li>Campo Maior</li>
            </ul>
            <ul id="go" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Goiânia</li>
                  <li>Aparecida de Goiânia</li>
                  <li>Anápolis</li>
                  <li>Rio Verde</li>
                  <li>Luziânia</li>
                  <li>Águas Lindas de Goiás</li>
                  <li>Valparaíso de Goiás</li>
                  <li>Trindade</li>
                  <li>Formosa</li>
                  <li>Novo Gama</li>
                  <li>Itumbiara</li>
                  <li>Senador Canedo</li>
                  <li>Catalão</li>
                  <li>Jataí</li>
                  <li>Planaltina</li>
                  <li>Caldas Novas</li>
            </ul>
            <ul id="ms" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Campo Grande</li>
                  <li>Dourados</li>
                  <li>Três Lagoas</li>
                  <li>Corumbá</li>
                  <li>Ponta Porã</li>
            </ul>
            <ul id="mt" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Cuiabá</li>
                  <li>Várzea Grande</li>
                  <li>Rondonópolis</li>
                  <li>Sinop</li>
                  <li>Tangará da Serra</li>
                  <li>Cáceres</li>
                  <li>Sorriso</li>
            </ul>
            <ul id="am" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Manaus</li>
                  <li>Parintins</li>
                  <li>Itacoatiara</li>
                  <li>Manacapuru</li>
                  <li>Coari</li>
                  <li>Centro Amazonense</li>
            </ul>
            <ul id="pa" class="hide" style="position: relative; top: 0px; left: 0px; display: none;">
                  <li>Belém</li>
                  <li>Ananindeua</li>
                  <li>Santarém</li>
                  <li>Marabá</li>
                  <li>Castanhal</li>
                  <li>Parauapebas</li>
                  <li>Itaituba</li>
                  <li>Cametá</li>
                  <li>Bragança&nbsp;</li>
                  <li>Abaetetuba</li>
                  <li>Bragança</li>
                  <li>Marituba</li>
            </ul>
      </div>
</div>