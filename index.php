
<?
$h1         = 'Compressores e Cia';
$title      = 'Cote com diversas empresas, compressores de varios tipos';
$desc       = 'Cotações de Compressores de ar com inumeras empresas | Entre em contato, é gratis!';
$var        = 'Home';
include('inc/head.php');

?>
</head>

<body>
	<? include('inc/topo.php'); ?>
	<section class="cd-hero">
		<div class="title-main">
			<h1><?= $h1 ?></h1>
		</div>
		<ul class="cd-hero-slider autoplay">
			<li class="selected">
				<div class="cd-full-width">
					<h2>Manutenção de Compressores</h2>
					<p>A manutenção de compressores é direcionada para evitar falhas que podem ocasionar problemas para produção e a segurança de quem utiliza nas operações. Por isso...
					</p>
					<a href="<?= $url ?>manutencao-de-compressores" class="cd-btn">Saiba mais</a>
				</div>
			</li>
			<li>
				<div class="cd-full-width">
					<h2>Compressor Parafuso</h2>
					<p>Sendo um equipamento extremamente eficiente, com um alto desempenho de trabalho e ótima durabilidade, o compressor parafuso é formado por dois rotores, que possuem um...</p>
					<a href="<?= $url ?>compressor-parafuso" class="cd-btn">Saiba mais</a>
				</div>
			</li>
			<li>
				<div class="cd-full-width">
					<h2>Compressor de ar odontológico</h2>
					<p>O compressor de ar odontológico é um dos principais equipamentos para os dentistas. Isso porque a maioria dos aparelhos utilizados pelos profissionais precisa do compressor para funcionar corretamente. </p>
					<a href="<?= $url ?>compressor-de-ar-odontologico" class="cd-btn">Saiba mais</a>
				</div>
			</li>
		</ul>
		<div class="cd-slider-nav">
			<nav>
				<span class="cd-marker item-1"></span>
				<ul>
					<li class="selected"><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
					<li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
					<li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
				</ul>
			</nav>
		</div>
	</section>
	<main>
		<section class="wrapper-main">
			<div class="main-center">
				<div class=" quadro-2 ">
					<h2 style="color:#1e3c72">Compressor de Ar Pistão</h2>
					<div class="div-img">
						<p>Um compressor de ar pistão é um tipo de compressor que usa um ou mais pistões para comprimir o ar. Ele funciona através da utilização de um pistão, que se move para cima e para baixo em um cilindro para comprimir o ar. O ar entra no cilindro quando o pistão se move para baixo, sendo comprimido quando o pistão se move para cima.</p>
					</div>
					<div class="gerador-svg">
						<img src="imagens/mpi/compressor-de-ar-preco-01.jpg" alt="Compressor de ar pistão" title="Compressor de ar pistão">
					</div>
				</div>
				<div class=" incomplete-box">
					<ul>
						<li>
							<p>É importante escolher o tipo adequado de compressor de ar pistão com base nas necessidades específicas de aplicação, considerando fatores como a demanda de ar, a pressão necessária e o ambiente de trabalho. Existem dois tipos de compressores que são os principais no mercado. Eles são:</p>

						<li><i class="fas fa-angle-right"></i>Compressor de Pistão Simples</li>
						<li><i class="fas fa-angle-right"></i>Compressor de Pistão Duplo</li>

					</ul>
					<a href="<?= $url ?>compressor-de-ar-pistao" class="btn-4">Saiba mais</a>
				</div>
			</div>
			<div id="content-icons">
				<div class="co-icon">
					<div class="quadro-icons">
						<i class="fas fa-wrench fa-7x"></i>
						<div>
							<p>Manutenção Especializada</p>
						</div>
					</div>
				</div>
				<div class="co-icon">
					<div class="quadro-icons">
						<i class="far fa-building fa-7x"></i>
						<div>
							<p>cotações com empresas distintas</p>
						</div>
					</div>
				</div>
				<div class="co-icon">
					<div class="quadro-icons">
						<i class="fas fa-clock fa-7x"></i>
						<div>
							<p>Rapidez no atendimento</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="wrapper-img">
			<div class="txtcenter">
				<h2>Produtos <b>Relacionados</b></h2>
			</div>
			<div class="content-icons">
				<div class="produtos-relacionados-1">
					<figure>
						<a href="<?= $url ?>compressor-de-ar">
							<div class="fig-img">
								<h2>Compresso de AR</h2>
								<div class="btn-5"> Saiba Mais </div>
							</div>
						</a>
					</figure>
				</div>
				<div class="produtos-relacionados-2">
					<figure class="figure2">
						<a href="<?= $url ?>aluguel-de-compressor">
							<div class="fig-img2">
								<h2>Aluguel de Compressor</h2>
								<div class="btn-5"> Saiba Mais </div>
							</div>
						</a>
					</figure>
				</div>
				<div class="produtos-relacionados-3">
					<figure>
						<a href="<?= $url ?>manutencao-de-compressores-industriais">
							<div class="fig-img">
								<h2>Manutenção de <br> Compressores Industriais</h2>
								<div class="btn-5"> Saiba Mais </div>
							</div>
						</a>
					</figure>
				</div>
			</div>
		</section>
		<section class="wrapper-destaque">
			<div class="destaque txtcenter">
				<h2>Galeria de <b>Produtos</b></h2>
				<div class="center-block txtcenter">
					<ul class="gallery">
						<li><a href="<?= $url ?>imagens/img-home/acessorios-para-.jpg" class="lightbox" title="acessorios-para-compressor.jpg">
								<img src="<?= $url ?>imagens/img-home/thumbs/acessorios-para-compressor.jpg" title="acessorios-para-compressor.jpg" alt="acessorios-para-compressor.jpg">
							</a>
						</li>
						<li><a href="<?= $url ?>imagens/img-home/aluguel-de-compressor.jpg" class="lightbox" title="aluguel-de-compressor.jpg">
								<img src="<?= $url ?>imagens/img-home/thumbs/aluguel-de-compressor.jpg" alt="aluguel-de-compressor.jpg" title="aluguel-de-compressor.jpg">
							</a>
						</li>
						<li><a href="<?= $url ?>imagens/img-home/aluguel-de-compressor-de-alta-pressao.jpg" class="lightbox" title="aluguel-de-compressor-de-alta-pressao.jpg">
								<img src="<?= $url ?>imagens/img-home/thumbs/aluguel-de-compressor-de-alta-pressao.jpg" alt="aluguel-de-compressor-de-alta-pressao.jpg" title="aluguel-de-compressor-de-alta-pressao.jpg">
							</a>
						</li>
						<li><a href="<?= $url ?>imagens/img-home/aluguel-de-compressor-de-ar.jpg" class="lightbox" title="aluguel-de-compressor-de-ar.jpg">
								<img src="<?= $url ?>imagens/img-home/thumbs/aluguel-de-compressor-de-ar.jpg" alt="aluguel-de-compressor-de-ar.jpg" title="aluguel-de-compressor-de-ar.jpg">
							</a>
						</li>
						<li><a href="<?= $url ?>imagens/img-home/aluguel-de-compressor-de-ar-industrial.jpg" class="lightbox" title="aluguel-de-compressor-de-ar-industrial.jpg">
								<img src="<?= $url ?>imagens/img-home/thumbs/aluguel-de-compressor-de-ar-industrial.jpg" alt="aluguel-de-compressor-de-ar-industrial.jpg" title="aluguel-de-compressor-de-ar-industrial.jpg">
							</a>
						</li>
						<li><a href="<?= $url ?>imagens/img-home/aluguel-de-compressor-de-ar-preco.jpg" class="lightbox" title="aluguel-de-compressor-de-ar-preco.jpg">
								<img src="<?= $url ?>imagens/img-home/thumbs/aluguel-de-compressor-de-ar-preco.jpg" alt="aluguel-de-compressor-de-ar-preco.jpg" title="aluguel-de-compressor-de-ar-preco.jpg">
							</a>
						</li>
						<li><a href="<?= $url ?>imagens/img-home/aluguel-de-compressores-de-ar-comprimido.jpg" class="lightbox" title="aluguel-de-compressores-de-ar-comprimido.jpg">
								<img src="<?= $url ?>imagens/img-home/thumbs/aluguel-de-compressores-de-ar-comprimido.jpg" alt="aluguel-de-compressores-de-ar-comprimido.jpg" title="aluguel-de-compressores-de-ar-comprimido.jpg">
							</a>
						</li>
						<li><a href="<?= $url ?>imagens/img-home/aluguel-de-compressor-industrial.jpg" class="lightbox" title="aluguel-de-compressor-industrial.jpg">
								<img src="<?= $url ?>imagens/img-home/thumbs/aluguel-de-compressor-industrial.jpg" alt="aluguel-de-compressor-industrial.jpg" title="aluguel-de-compressor-industrial.jpg">
							</a>
						</li>
						<li><a href="<?= $url ?>imagens/img-home/aluguel-de-compressor-parafuso.jpg" class="lightbox" title="aluguel-de-compressor-parafuso.jpg">
								<img src="<?= $url ?>imagens/img-home/thumbs/aluguel-de-compressor-parafuso.jpg" alt="aluguel-de-compressor-parafuso.jpg" title="aluguel-de-compressor-parafuso.jpg">
							</a>
						</li>
						<li><a href="<?= $url ?>imagens/img-home/aluguel-de-compressor-preco.jpg" class="lightbox" title="aluguel-de-compressor-preco.jpg">
								<img src="<?= $url ?>imagens/img-home/thumbs/aluguel-de-compressor-preco.jpg" alt="aluguel-de-compressor-preco.jpg" title="aluguel-de-compressor-preco.jpg">
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="wrapper">
				<h2>Produtos mais vendidos</h2>
				<ul class="thumbnails-mod1">
					<li>
						<a href="<?= $url; ?>compressor-de-ar-pistao" title="COMPRESSOR DE AR PISTÃO"><img src="<?= $url; ?>imagens/mpi/thumbs/compressor-de-ar-preco-01.jpg" alt="COMPRESSOR DE AR PISTÃO" title="COMPRESSOR DE AR PISTÃO" /></a>
						<h2><a href="<?= $url; ?>compressor-de-ar-pistao" title="COMPRESSOR DE AR PISTÃO">COMPRESSOR DE AR PISTÃO</a></h2>
					</li>
					<li>
						<a href="<?= $url; ?>compressor-de-ar-de-parafuso-sp" title="COMPRESSOR DE AR DE PARAFUSO SP"><img src="<?= $url; ?>imagens/compressores/thumbs/compressores-5.jpg" alt="COMPRESSOR DE AR DE PARAFUSO SP" title="COMPRESSOR DE AR DE PARAFUSO SP" /></a>
						<h2><a href="<?= $url; ?>compressor-de-ar-de-parafuso-sp" title="COMPRESSOR DE AR DE PARAFUSO SP">COMPRESSOR DE AR DE PARAFUSO SP</a></h2>
					</li>
					<li>
						<a href="<?= $url; ?>pecas-para-compressor" title="PEÇAS PARA COMPRESSOR"><img src="<?= $url; ?>imagens/compressores/thumbs/compressores-7.jpg" alt="PEÇAS PARA COMPRESSOR" title="PEÇAS PARA COMPRESSOR" /></a>
						<h2><a href="<?= $url; ?>pecas-para-compressor" title="PEÇAS PARA COMPRESSOR">PEÇAS PARA COMPRESSOR</a></h2>
					</li>
					<li>
						<a href="<?= $url; ?>compressor-industrial-parafuso" title="COMPRESSOR INDUSTRIAL PARAFUSO"><img src="<?= $url; ?>imagens/compressores/thumbs/compressores-4.jpg" alt="COMPRESSOR INDUSTRIAL PARAFUSO" title="COMPRESSOR INDUSTRIAL PARAFUSO" /></a>
						<h2><a href="<?= $url; ?>compressor-industrial-parafuso" title="COMPRESSOR INDUSTRIAL PARAFUSO">COMPRESSOR INDUSTRIAL PARAFUSO</a></h2>
					</li>
				</ul>


			</div>
		</section>
	</main>
	<? include('inc/footer.php'); ?>
	<link rel="stylesheet" href="<?= $url ?>nivo/nivo-slider.css" media="screen">
	<script src="<?= $url ?>nivo/jquery.nivo.slider.js"></script>
	<script>
		$(window).load(function() {
			$('#slider').nivoSlider();
		});
	</script>
	<script src="<?= $url ?>hero/js/modernizr.js"></script>
	<script src="<?= $url ?>hero/js/main.js"></script>
</body>

</html>
