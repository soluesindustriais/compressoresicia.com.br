'use strict'

var popup = document.getElementById('popup')
var btn_submit_two = document.getElementById('btn-modal-exit')
var origin_lead = document.getElementById('lead-origin-two')
var next_page = document.getElementById('next-page-two')
var screen_width = window.screen.width
var screen_height = window.screen.height

//componentes de notificação
var notification = document.getElementById('notification')
var progress_cover = document.getElementById('progress-cover')
var progress_content = document.getElementById('progress-content')

var modal_button = document.getElementById('btn-modal')
var close_notify = document.getElementById('close-notify')



var screen_size = document.getElementById('screen-size-two')



// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
var close_btn = document.getElementById('btn-modal-exit')

// When the user clicks the button, open the modal 
function open_modal() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

close_btn.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

$(document).ready(function() {
    $('#fone-modal').mask('(99) 9999-9999');
    $('#cnpj-modal').mask('99.999.999/9999-99');
    $('#cell-modal').mask('(99) 9999-99999');
});

//aceitar apenas numeros nos inputs
function onlynumber(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    //var regex = /^[0-9.,]+$/;
    var regex = /^[0-9.]+$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

//Capturando tamanho da tela para analise
screen_size.value = screen_width + ' x ' + screen_height

//Capturando a página da cotação
origin_lead.value = window.location.href

/* Fechar notificação
close_notify.addEventListener('click', function() {
    notification.style.display = 'none'
    progress_cover.style.display = 'none'
})


modal_button.addEventListener('click', function() {
    Notification.display = 'block'
    progress_cover.display = 'block'
    alert('Cotação enviada')
})

//fechar notificação após 10 segundos (10000 ms)
setInterval(function() {
    notification.style.display = 'none'
    progress_cover.style.display = 'none'
}, 5000)

setInterval(function() {

    do {
        progress_content.style.width = i + '%'
        i++;
    }
    while (i < 30);


}, 50)
*/

next_page.value = window.location.href