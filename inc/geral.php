<?


$nomeSite			= 'Compressores e Cia';
$slogan				= 'Cotações gratuitas de Compresores de AR';

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") {
	$url = $http . "://" . $host . "/";
} else {
	$url = $http . "://" . $host . $dir["dirname"] . "/";
}

$emailContato		= 'everton.lima@solucoesindustriais.com.br';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php', '', $urlPagina);
$urlPagina 			== "index" ? $urlPagina = "" : "";
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
//Breadcrumbs
$caminho 			= '
<div class="breadcrumb">
<div class="wrapper">
	<div class="bread__row">
		<nav aria-label="breadcrumb">
			<ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
				<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
					<a href="' . $url . '" itemprop="item" title="Home">
						<span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home » </span>
					</a>
					<meta itemprop="position" content="1" />
				</li>
				<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
					<span itemprop="name">' . $h1 . '</span>
					<meta itemprop="position" content="2" />
				</li>
			</ol>
		</nav>
	</div>
</div>
</div>
';
$caminho2	= '
<div class="breadcrumb">
<div class="wrapper">
    <div class="bread__row">
<nav aria-label="breadcrumb">
<ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a href="' . $url . '" itemprop="item" title="Home">
    <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home »  </span>
  </a>
  <meta itemprop="position" content="1" />
</li>
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
    <span itemprop="name">Produtos »  </span>
  </a>
  <meta itemprop="position" content="2" />
</li>   
<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <span itemprop="name">' . $h1 . '</span>
  <meta itemprop="position" content="3" />
</li>
</ol>
</nav>
</div>
</div>
</div>
';

$caminhoinformacoes	= '
<div class="breadcrumb">
<div class="wrapper">
    <div class="bread__row">
<nav aria-label="breadcrumb">
<ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a href="' . $url . '" itemprop="item" title="Home">
    <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home »  </span>
  </a>
  <meta itemprop="position" content="1" />
</li>
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'informacoes" itemprop="item" title="Informações">
    <span itemprop="name">Informações »  </span>
  </a>
  <meta itemprop="position" content="2" />
</li>   
<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <span itemprop="name">' . $h1 . '</span>
  <meta itemprop="position" content="3" />
</li>
</ol>
</nav>
</div>
</div>
</div>
';

$caminhomolas_de_compressao	= '
<div class="breadcrumb">
<div class="wrapper">
    <div class="bread__row">
<nav aria-label="breadcrumb">
<ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a href="' . $url . '" itemprop="item" title="Home">
    <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home »  </span>
  </a>
  <meta itemprop="position" content="1" />
</li>
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
    <span itemprop="name">Produtos »  </span>
  </a>
  <meta itemprop="position" content="2" />
</li>   
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'molas-de-compressao-categoria" itemprop="item" title="Molas de Compressão">
    <span itemprop="name">Molas de Compressão »  </span>
  </a>
  <meta itemprop="position" content="3" />
</li> 
<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <span itemprop="name">' . $h1 . '</span>
  <meta itemprop="position" content="4" />
</li>
</ol>
</nav>
</div>
</div>
</div>
';

$caminhocompressores	= '
<div class="breadcrumb">
<div class="wrapper">
    <div class="bread__row">
<nav aria-label="breadcrumb">
<ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a href="' . $url . '" itemprop="item" title="Home">
    <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home »  </span>
  </a>
  <meta itemprop="position" content="1" />
</li>
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
    <span itemprop="name">Produtos »  </span>
  </a>
  <meta itemprop="position" content="2" />
</li>   
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'compressores-categoria" itemprop="item" title="Compressores">
    <span itemprop="name">Compressores »  </span>
  </a>
  <meta itemprop="position" content="3" />
</li> 
<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <span itemprop="name">' . $h1 . '</span>
  <meta itemprop="position" content="4" />
</li>
</ol>
</nav>
</div>
</div>
</div>';


$caminhogeradores	= '
<div class="breadcrumb">
<div class="wrapper">
    <div class="bread__row">
<nav aria-label="breadcrumb">
<ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a href="' . $url . '" itemprop="item" title="Home">
    <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home »  </span>
  </a>
  <meta itemprop="position" content="1" />
</li>
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
    <span itemprop="name">Produtos »  </span>
  </a>
  <meta itemprop="position" content="2" />
</li>   
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'geradores-categoria" itemprop="item" title="Geradores">
    <span itemprop="name">Geradores »  </span>
  </a>
  <meta itemprop="position" content="3" />
</li> 
<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <span itemprop="name">' . $h1 . '</span>
  <meta itemprop="position" content="4" />
</li>
</ol>
</nav>
</div>
</div>
</div>
';


$caminhorede_de_ar_pneumatica	= '
<div class="breadcrumb">
<div class="wrapper">
    <div class="bread__row">
<nav aria-label="breadcrumb">
<ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a href="' . $url . '" itemprop="item" title="Home">
    <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home »  </span>
  </a>
  <meta itemprop="position" content="1" />
</li>
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
    <span itemprop="name">Produtos »  </span>
  </a>
  <meta itemprop="position" content="2" />
</li>   
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'rede-de-ar-pneumatica-categoria" itemprop="item" title="Rede de Ar Pneumática">
    <span itemprop="name">Rede de Ar Pneumática »  </span>
  </a>
  <meta itemprop="position" content="3" />
</li> 
<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <span itemprop="name">' . $h1 . '</span>
  <meta itemprop="position" content="4" />
</li>
</ol>
</nav>
</div>
</div>
</div>';


$caminhomanutencao_de_compressor	= '
<div class="breadcrumb">
<div class="wrapper">
    <div class="bread__row">
<nav aria-label="breadcrumb">
<ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a href="' . $url . '" itemprop="item" title="Home">
    <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home »  </span>
  </a>
  <meta itemprop="position" content="1" />
</li>
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
    <span itemprop="name">Produtos »  </span>
  </a>
  <meta itemprop="position" content="2" />
</li>   
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'manutencao-de-compressor-categoria" itemprop="item" title="Manutenção de Compressor">
    <span itemprop="name">Manutenção de Compressor »  </span>
  </a>
  <meta itemprop="position" content="3" />
</li> 
<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <span itemprop="name">' . $h1 . '</span>
  <meta itemprop="position" content="4" />
</li>
</ol>
</nav>
</div>
</div>
</div>
';
