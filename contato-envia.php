<?php 
date_default_timezone_set("America/Sao_Paulo");
// Verifico se foi feita a postagem do Captcha

    //Atenticador do e-mail com SSL
    //require_once('inc/contato/mail.send.php');

    //Informações que serão gravadas no isereleads
    $urlsite = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

    $recebenome = $post["nome"];
    $recebemail = $post["email"];
    $recebetelefone = $post["telefone"];
    $recebemensagem = strip_tags(trim($post["mensagem"]));
    $dataEnvio = date('d/m/Y H:i:s');

    // MENSAGEM 
    $corpo = null;
    $corpo .= "<table style='border-collapse:collapse;border-spacing:0;border-color:#761919'>
              <tr>
                <th style='font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;border-top-width:1px;border-bottom-width:1px;vertical-align:top;text-align: center;' colspan='2'>
                  <a href='{$url}' title='{$nomeSite}'><img src='{$url}/imagens/logo-email.png' width='85' title='{$nomeSite}' alt='{$nomeSite}'></a>
                </th>
              </tr>
              
              <tr>
                <th style='font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;border-top-width:1px;border-bottom-width:1px;vertical-align:top;text-align: center;' colspan='2'>
                  Mensagem recebida de {$recebenome}, via formulário do site.
                </th>
              </tr>
              
              <tr>";
    foreach ($post as $key => $value):
      $corpo .= "<tr>
              <td style='font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;background-color:#f9f9f9;border-top-width:1px;border-bottom-width:1px;vertical-align:top;border-right:1px solid #ccc;'>
                <b>" . strtoupper(str_replace(array('_', '-'), ' ', $key)) . ": </b>
              </td>
              <td style='font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f9f9f9;border-top-width:1px;border-bottom-width:1px;vertical-align:top'>
                {$value}
              </td>
              </tr>";
    endforeach;
    $corpo .= "</tr>   
              <tr>
                <td style='text-align:center;font-family:Arial, sans-serif;font-size:9px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;border-top-width:1px;border-bottom-width:1px;text-align:center;vertical-align:top' colspan='2'>
                  Mensagem automática enviada por - {$nomeSite} em {$dataEnvio}
                </td>
              </tr>
              <tr>
                <td style='text-align:center;font-family:Arial, sans-serif;font-size:9px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;border-top-width:1px;border-bottom-width:1px;text-align:center;vertical-align:top' colspan='2'>
                  <a href='{$url}' title='{$nomeSite}'>{$url}</a>
                </td>
              </tr>
            </table>";

try {
$ant = json_decode('{"entidade":"2","pessoa":{"nome":""},"empresa":{"titulo":"","site":""},"telefones_pessoas":[{"numero":""}],"emails_pessoas":[{"endereco":""}],"enderecos_pessoas":[{"cep":"","endereco":""}],"pessoas_interesses":[{"unic_key": "","empresa":{"titulo":"","site":""},"interesse":{"titulo":""},"origem":"","conteudo":""}],"cargo":{"titulo":""}}');
$ant->pessoa->nome = $post['nome'];
$ant->telefones_pessoas[0] = ["numero" => $post['telefone']];
$ant->emails_pessoas[0] = ["endereco" => $post['email']];
$ant->pessoas_interesses[0]->interesse->titulo = $urlsite;
$ant->pessoas_interesses[0]->empresa->titulo = $post['site'];
$ant->pessoas_interesses[0]->conteudo = $recebemensagem;
$ant->pessoas_interesses[0]->origem = $_SERVER['HTTP_REFERER'];
$ant->pessoas_interesses[0]->empresa->site = $_SERVER['HTTP_ORIGIN'];
$ant = json_encode($ant, JSON_UNESCAPED_UNICODE);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://ant.idealtrends.com.br/api/leads");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $ant);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
$resposta = curl_exec($ch);
curl_close($ch);
$resposta = json_decode($resposta, true);

} catch (\Exception $e) {
 
}

    //ENVIO EMPRESA
    $mail->From = $EMAIL; // Remetente
    $mail->FromName = $post['nome']; // Remetente nome
    $mail->Sender = $EMAIL; // Seu e-mail

    $mail->AddAddress($emailContato, $EMPRESA); // Destinatário principal
    //$mail->AddCC('adm@site.com.br', 'Teste'); // Copia
    $mail->AddBCC('guilherme.solucoesindustriais@gmail.com', 'Guilherme'); // Cópia Oculta
    $mail->AddReplyTo($post['email'], $post['nome']); // Reply-to
    $mail->Subject = $EMPRESA . ': Contato pelo site'; // Assunto da mensagem
    $mail->Body = $corpo; // corpo da mensagem
    $enviaEmpresa = $mail->Send(); // Enviando o e-mail
    $mail->ClearAllRecipients(); // Limpando os destinatários
    $mail->ClearAttachments(); // Limpando anexos
    
    // ENVIO USUÁRIO
    $mail->From = $recebemail; // Remetente
    $mail->FromName = $EMPRESA; // Remetente nome
    $mail->Sender = $EMAIL; // Seu e-mail
    $mail->AddAddress($post['email'], $post['nome']); // Destinatário principal

    $mail->Subject = $EMPRESA . ': Recebemos sua mensagem'; // Assunto da mensagem
    $mail->Body = $corpo; // corpo da mensagem
    $mail->Send(); // Enviando o e-mail
    $mail->ClearAllRecipients(); // Limpando os destinatários
    $mail->ClearAttachments(); // Limpando anexos

    // echo '<pre>';
    // print_r($mail);
    // echo '</pre>';
    //  exit;

    if ($enviaEmpresa):
      echo '<script>'
      . '$(function () {';
      echo 'swal({
            title: "Tudo certo!",
            text: "Obrigado por entrar em contato, sua mensagem foi enviada com sucesso",
            type: "success",
            showCancelButton: false,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ok",
            closeOnConfirm: true
          });';
      echo '});'
      . '</script>';
    else:
      echo '<script>'
      . '$(function () {';
      echo 'swal("Opsss!", "Desculpe, mas houve um erro ao enviar a mensagem, por favor tente novamente.", "error");';
      echo '});'
      . '</script>';
    endif;
    
 