<div class="logo-top">
  <a href="<?=$url?>" title="início">
  <img class="lazyload" src="imagens/img-home/logo.png" alt="<?=$nomeSite?>" title="<?=$nomeSite?>"></a>
</div>
<ul>
  <li><a href="<?=$url?>" title="Página inicial">Início</a></li>
  <li class="dropdown"><a href="<?=$url?>informacoes" title="Produtos relacionados">Informações</a>
  <ul class="sub-menu-info">
    <? include('inc/sub-menu.php');?>
  </ul>
  </li>
  <li class="dropdown"><a href="<?=$url?>produtos" title="produtos">Produtos</a>

  <ul class="sub-menu">
  <li class="dropdown-2"><a href="<?=$url?>molas-de-compressao-categoria" title="Molas de Compressão">Molas de Compressão</a>
<ul class="sub-menu2 lista-interna">
		<? include('inc/molas-de-compressao/molas-de-compressao-sub-menu.php');?>
	</ul>
</li>

<li class="dropdown-2"><a href="<?=$url?>compressores-categoria" title="Compressores">Compressores</a>
<ul class="sub-menu2 lista-interna">
		<? include('inc/compressores/compressores-sub-menu.php');?>
	</ul>
</li>

<li class="dropdown-2"><a href="<?=$url?>geradores-categoria" title="Geradores">Geradores</a>
<ul class="sub-menu2 lista-interna">
		<? include('inc/geradores/geradores-sub-menu.php');?>
	</ul>
</li>

<li class="dropdown-2"><a href="<?=$url?>rede-de-ar-pneumatica-categoria" title="Rede de Ar Pneumática">Rede de Ar Pneumática</a>
<ul class="sub-menu2 lista-interna">
		<? include('inc/rede-de-ar-pneumatica/rede-de-ar-pneumatica-sub-menu.php');?>
	</ul>
</li>

<li class="dropdown-2"><a href="<?=$url?>manutencao-de-compressor-categoria" title="Manutenção de Compressor">Manutenção de Compressor</a>
<ul class="sub-menu2 lista-interna">
		<? include('inc/manutencao-de-compressor/manutencao-de-compressor-sub-menu.php');?>
	</ul>
</li>

  </ul>
  </li>

  
  <li><a href="<?=$url?>sobre-nos" title="Sobre nós"> Sobre nós</a></li>
  <li><a href="<?=$url?>blog" title="Blog"> Blog</a></li>
  <li><a href="https://faca-parte.solucoesindustriais.com.br/" title="Faça parte" target="_blank" class="faca-parte"> Faça parte</a></li>
<!-- <li>
  <div class="google-search">
    <div>
      
      <script>
      (function() {
      var cx = '004602583560596900818:ulvp7knp9su';
      var gcse = document.createElement('script');
      gcse.type = 'text/javascript';
      gcse.async = true;
      gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(gcse, s);
      })();
      </script>
      <div class="gcse-searchbox-only"></div>
    </div>
  </div>
</li> -->