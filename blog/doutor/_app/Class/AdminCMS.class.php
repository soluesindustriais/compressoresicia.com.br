<?php
/**
* AdminCMS.class [ CLASSE ]
* Classe resposável por gerir as empresas e usuários no admin da CMS
* @copyright (c) 2016, Rafael da Silva Lima - Inove Dados
*/
class AdminCMS {
//Tratamento de resultados e mensagens
    private $Result;
    private $Error;
//Entrada de dados
    private $Data;
    private $Id;
//Set de dados
    private $Empresa;
    private $Logo;
    private $User;
    private $Mensagem;
/**
* <b>Cadastrar empresa:</b>
* Envelopa os dados de um array atribuitivo
* @param array $Data = Dados vindos de um post de formulário em array
*/
public function ExeCreate(array $Data) {
    $this->Data = $Data;
    $this->CropData();
    $this->CheckUnset();
    $this->CheckDataCreate();
    if ($this->Result):
        $this->CadEmp();
    endif;
}
/**
* <b>Atualiza empresa:</b>
* Envelopa os dados de um array atribuitivo
* @param array $Data = Dados vindos de um post de formulário em array
*/
public function ExeUpdate($EmpId, array $Data) {
    $this->Data = $Data;
    $this->Id = (int) $EmpId;
    $this->CropData();
    $this->CheckUnset();
    $this->CheckDataUpdate();
    if ($this->Result):
        $this->UpEmp();
    endif;
}
/**
* <b>Retorno de consulta</b>
* Se não houve consulta ele retorna false bolean
* @return object = consulta do webservice envelopada
*/
public function getResult() {
    return $this->Result;
}
/**
* <b>Mensagens do sistema</b>
* Mensagem e tipo de mensagem [0] e [1] pode ser die entre eles.
* @return array = mensagem do sistema, utilizar o gatilho de erros do sistema para exibir em tela.
*/
public function getError() {
    return $this->Error;
}
########################################
########### METODOS PRIVADOS ###########
########################################
//Tratar todos os dados que estão entrando e prepara-los para inserção no banco de dados
//Separa os dados do usuario e os dados da empresa e esvazia o atributo $Data
private function CropData() {
    $data = $this->Data;
    unset($this->Data['user_email'], $this->Data['user_password'], $this->Data['user_name'], $this->Data['user_lastname'], $this->Data['user_genero'], $this->Data['user_nasc'], $this->Data['user_fone'], $this->Data['user_ramal'], $this->Data['user_level'], $this->Data['user_cargo']);
    $this->Empresa = $this->Data;
    unset($data['empresa_name'], $data['empresa_razaosocial'], $data['empresa_cnpj'], $data['empresa_ie'], $data['empresa_ramo'], $data['empresa_sobre'], $data['empresa_uf'], $data['empresa_cidade'], $data['empresa_endereco'], $data['empresa_cep'], $data['empresa_fone'], $data['empresa_site'], $data['empresa_disco'], $data['empresa_users'], $data['empresa_capa']);
    $this->User = $data;
    $this->Data = null;
}
//Metodo para verificar se os campos opcionais estão vazios e dar unset em sua chave no array.
private function CheckUnset() {
    if (empty($this->Empresa['empresa_capa'])):
        unset($this->Empresa['empresa_capa']);
    else:
        $this->Logo = $this->Empresa['empresa_capa'];
        unset($this->Empresa['empresa_capa']);
    endif;
    if (empty($this->Empresa['empresa_site'])):
        unset($this->Empresa['empresa_site']);
    endif;
    if (empty($this->Empresa['empresa_sobre'])):
        unset($this->Empresa['empresa_sobre']);
    endif;
    if (empty($this->User['user_ramal'])):
        unset($this->User['user_ramal']);
    endif;
}
//Verifica os dados e filtra as informações para o envelope de arrays
private function CheckDataCreate() {
    if (in_array('', $this->User) && in_array('', $this->Empresa)):
        $this->Result = false;
    $this->Error = array("Existem dados obrigatórios não preenchidos. Revise os dados e tente novamente.", WS_ERROR, "Doutores da Web");
elseif (!Check::Email($this->User['user_email'])):
    $this->Error = array("Digite um endereço de e-mail válido.", WS_ERROR, "Doutores da Web");
    $this->Result = false;
else:
    $this->User['user_nasc'] = Check::Data($this->User['user_nasc']);
    $this->User['user_registration'] = date('Y-m-d H:i:s');
    $this->Empresa['empresa_title'] = Check::Name($this->Empresa['empresa_name']);
    $this->Empresa['empresa_date'] = date('Y-m-d H:i:s');
    $this->CheckEmail();
endif;
}
//Verifica os dados e filtra as informações para o envelope de arrays
private function CheckDataUpdate() {
    if (in_array('', $this->Empresa)):
        $this->Result = false;
        $this->Error = array("Existem dados obrigatórios não preenchidos. Revise os dados e tente novamente.", WS_ERROR, "Doutores da Web");
    else:
        $this->Empresa['empresa_title'] = Check::Name($this->Empresa['empresa_name']);
        $this->Empresa['empresa_lastupdate'] = date('Y-m-d H:i:s');
        $this->CheckEmp();
    endif;
}
//Verifica usuário pelo e-mail, Impede cadastro duplicado!
private function CheckEmail() {
    $readUser = new Read;
    $readUser->ExeRead(TB_USERS, "WHERE user_email = :email", "email={$this->User['user_email']}");
    if ($readUser->getResult()):
        $this->Error = array("Digite um endereço de e-mail diferente.", WS_ERROR, "Doutores da Web");
        $this->Result = false;
    else:
        $this->Result = true;
        $this->CheckEmp();
    endif;
}
//Verifica se a empresa já existe
private function CheckEmp() {
    $where = ( isset($this->Id) ? "empresa_id != {$this->Id} AND" : '' );
    $readEmp = new Read;
    $readEmp->ExeRead(TB_EMP, "WHERE {$where} empresa_cnpj = :pj", "pj={$this->Empresa['empresa_cnpj']}");
    if ($readEmp->getRowCount()):
        $this->Error = array("Já existe uma empresa com o CNPJ <b>{$this->Empresa['empresa_cnpj']}</b> cadastrado.", WS_ERROR, "Doutores da Web");
        $this->Result = false;
    else:
        $this->Result = true;
    endif;
}
//Verifica e envia a imagem da empresa para pasta da (int) empresa/user (desativada)
private function SendLogo() {
    if (!empty($this->Logo['tmp_name'])):
//            $resize = getimagesize($this->Logo['tmp_name']);
//            if ($resize[0] != 578 && $resize[1] != 288):
//                $this->Error = array('A imagem deve ter 578x288px e ser do tipo .JPG, .PNG ou .GIF!', WS_INFOR, "Doutores da Web");
//                $this->Result = false;
//            else:
        $Upload = new Upload;
        $ImgName = "emp-{$this->Empresa['empresa_name']}-" . (substr(md5(time() + $this->Empresa['empresa_razaosocial']), 0, 5));
        $Upload->Image($this->Logo, $ImgName, 350, null, 'logo');
        if ($Upload->getError()):
            $this->Error = array($Upload->getError(), WS_ERROR, 'Alerta!');
            $this->Result = false;
        else:
            $this->checkCover();
            $this->Logo = array('empresa_capa' => $Upload->getResult());
            $this->UpdateLogo();
        endif;
//            endif;
    endif;
}
//Verifica se já existe uma logo, se sim deleta para enviar outra!
private function checkCover() {
    $readCapa = new Read;
    $readCapa->FullRead("SELECT empresa_capa FROM " . TB_EMP . " WHERE empresa_id = :id", "id={$this->Id}");
    if ($readCapa->getRowCount()):
        $delCapa = $readCapa->getResult();
        $delCapa = $delCapa[0]['empresa_capa'];
        if (file_exists("uploads/{$delCapa}") && !is_dir("uploads/{$delCapa}")):
            unlink("uploads/{$delCapa}");
    endif;
endif;
}
//Após o cadastro da empresa e o envio da imagem para o diretorio é necessário fazer um Update e adicionar o link da imagem no banco (Desativada)
private function UpdateLogo() {
    $UpdateLogo = new Update;
    $UpdateLogo->ExeUpdate(TB_EMP, $this->Logo, "WHERE empresa_id = :id", "id={$this->Id}");
    if (!$UpdateLogo->getResult()):
        $this->Result = false;
        $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ALERT, "Doutores da Web");
    endif;
}
//Atualiza a empresa e retorna a id cadastrada para o atributo $User[]
private function UpEmp() {
    $UpEmp = new Update;
    $UpEmp->ExeUpdate(TB_EMP, $this->Empresa, "WHERE empresa_id = :id", "id={$this->Id}");
    if ($UpEmp->getRowCount() == 0):
        $this->Result = false;
        $this->Error = array("Nenhuma alteração foi realizada.", WS_INFOR, "Doutores da Web");
    else:
        $this->SendLogo();
        $this->SendUpdate();
    endif;
}
//Cadastra a empresa e retorna a id cadastrada para o atributo $User[]
private function CadEmp() {
    $CadEmp = new Create;
    $CadEmp->ExeCreate(TB_EMP, $this->Empresa);
    if (!$CadEmp->getResult()):
        $this->Result = false;
        $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Doutores da Web");
    else:
        $this->Id = $CadEmp->getResult();
        $this->SendLogo();
        $this->CadUser();
    endif;
}
//Cadastra o usuário
private function CadUser() {
    $user_pass = $this->User['user_password'];
    $this->User['user_password'] = md5($user_pass);
    $this->User['user_empresa'] = $this->Id;
    $cadUser = new Create;
    $cadUser->ExeCreate(TB_USERS, $this->User);
    if (!$cadUser->getResult()):
        $this->Result = false;
        $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Doutores da Web");
    else:
        $this->Result = true;
        $this->User['user_pass'] = $user_pass;
        $this->SendWelcome();
    endif;
}
//Envia e-mail de boas vindas para a empresa
private function SendWelcome() {
//Paramentos a serem enviados dentro do template view formato Array
    $this->User['MENSAGEM'] = BOAS_VINDAS;
    $this->User['SITENAME'] = SITENAME;
    $this->User['SITEDESC'] = SITEDESC;
    $view = new View;
    $tpl = $view->Load('email/cadastroempresa');

    $SendMail = new Email;
//Define o cabeçalho e a mensagem do e-mail que pode ser vinda de um tpl
    $this->Mensagem['Assunto'] = 'Empresa cadastrada - ' . SITENAME;
    $this->Mensagem['DestinoNome'] = $this->User['user_name'] . ' ' . $this->User['user_lastname'];
    $this->Mensagem['DestinoEmail'] = $this->User['user_email'];
    $this->Mensagem['RemetenteNome'] = REMETENTENOME;
    $this->Mensagem['RemetenteEmail'] = REMETENTEMAIL;
    $this->Mensagem['Mensagem'] = $view->Show($this->User, $tpl);
    $SendMail->Enviar($this->Mensagem);
    if (!$SendMail->getResult()):
        $this->Error = array("<p>A empresa <b>{$this->Empresa['empresa_razaosocial']}</b> e o usuário <b>{$this->User['user_name']} {$this->User['user_lastname']}</b> foram cadastrados com sucesso.</p>", WS_INFOR, "Doutores da Web");
        Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Empresas", "Cadastrou a empresa {$this->Empresa['empresa_razaosocial']}", date("Y-m-d H:i:s"));
    else:
        $this->Error = array("<p>A empresa <b>{$this->Empresa['empresa_razaosocial']}</b> e o usuário <b>{$this->User['user_name']} {$this->User['user_lastname']}</b> foram cadastrados com sucesso.</p>", WS_ACCEPT, "Doutores da Web");
        Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Empresas", "Cadastrou a empresa {$this->Empresa['empresa_razaosocial']}", date("Y-m-d H:i:s"));
        $this->Empresa = null;
        $this->User = null;
        $this->Logo = null;
        $this->Mensagem = null;
    endif;
}
//Envia e-mail de atualziação da empresa com todos os dados
private function SendUpdate() {
//Paramentos a serem enviados dentro do template view formato Array
    $this->Empresa['MENSAGEM'] = UPDATE_EMPRESA;
    $this->Empresa['SITENAME'] = SITENAME;
    $this->Empresa['SITEDESC'] = SITEDESC;
    $SendMail = new Email;
    $ReadAdm = new Read;
    $ReadAdm->ExeRead(TB_USERS, "WHERE user_empresa = :id AND user_level >= :lv", "id={$this->Id}&lv=3}");
    $view = new View;
    $tpl = $view->Load('email/updateempresa');
    foreach ($ReadAdm->getResult() as $key):
        extract($key);
        $this->Empresa['user_name'] = $user_name;
        $this->Empresa['user_lastname'] = $user_lastname;
        $this->Empresa['empresa_cityuf'] = Check::City($this->Empresa['empresa_cidade']);
//Define o cabeçalho e a mensagem do e-mail que pode ser vinda de um tpl para todos os usuários level 3 da empresa
        $this->Mensagem['Assunto'] = 'Dados da empresa atualizados - ' . SITENAME;
        $this->Mensagem['DestinoNome'] = $user_name . ' ' . $user_lastname;
        $this->Mensagem['DestinoEmail'] = $user_email;
        $this->Mensagem['RemetenteNome'] = REMETENTENOME;
        $this->Mensagem['RemetenteEmail'] = REMETENTEMAIL;
        $this->Mensagem['Mensagem'] = $view->Show($this->Empresa, $tpl);
        $SendMail->Enviar($this->Mensagem);
    endforeach;
    if (!$SendMail->getResult()):
        $this->Error = array("<p>Os dados da empresa <b>{$this->Empresa['empresa_razaosocial']}</b> foram atualizados com sucesso.</p>", WS_INFOR, "Doutores da Web");
        Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Empresas", "Atualizou a empresa {$this->Empresa['empresa_razaosocial']}", date("Y-m-d H:i:s"));
    else:
        $this->Error = array("Os dados da empresa <b>{$this->Empresa['empresa_razaosocial']}</b> foram atualizados com sucesso.", WS_ACCEPT, "Doutores da Web");
        Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Empresas", "Atualizou a empresa {$this->Empresa['empresa_razaosocial']}", date("Y-m-d H:i:s"));
        $this->Empresa = null;
        $this->User = null;
        $this->Logo = null;
        $this->Mensagem = null;
        $this->Id = null;
    endif;
}
}