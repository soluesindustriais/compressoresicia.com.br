function toggleReadMore() {

    var content = document.querySelector('.article-content');
    var readMoreButton = document.querySelector('.read-more-button');
    var closeButton = document.querySelector('.close-button');


    if (content.style.maxHeight) {
        content.style.maxHeight = null;
        closeButton.style.display = 'none';
        readMoreButton.style.display = 'block';
    } else {
        content.style.maxHeight = "none";
        closeButton.style.display = 'block';
        readMoreButton.style.display = 'none';
    }
}