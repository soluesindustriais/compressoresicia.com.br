<?
$h1         = 'Produtos';
$title      = 'Produtos';
$desc       = 'Encontre diversos equipamentos e peças automotivas das melhores empresas. Receba diversos comparativos pelo formulário com mais de 200 fornecedores. É grátis!';
$key        = 'produtos, materiais, equipamentos';
$var        = 'produtos';
include('inc/head.php');
?>
</head>

<body>

  <? include('inc/topo.php'); ?>
  <div class="wrapper">
    <main>
      <div class="content">
        <?php echo "$caminho"?>
        
        <h1>Produtos</h1>
        <article class="full">
          <p>Encontre diversos produtos de aço das melhores empresas, para suas necessidades. Receba diversos comparativos pelo formulário com mais de 200 fornecedores.</p>
          <ul class="thumbnails-main">

            <li>
              <a rel="nofollow" href="<?= $url ?>molas-de-compressao-categoria" title="Molas de Compressão"><img src="imagens/molas-de-compressao-01.jpg" alt="Molas de Compressão" title="Molas de Compressão" /></a>
              <h2><a href="<?= $url ?>molas-de-compressao-categoria" title="Molas de Compressão">Molas de Compressão</a></h2>
            </li>

            <!--brendo<li>
            <a rel="nofollow" href="<?= $url ?>artefato-e-pecas-de-arame-categoria" title="Artefato e Peças de Arame"><img src="imagens/artefato-e-pecas-de-arame-01.jpg" alt="Artefato e Peças de Arame" title="Artefato e Peças de Arame"/></a>
            <h2><a href="<?= $url ?>artefato-e-pecas-de-arame-categoria" title="Artefato e Peças de Arame ">Artefato e Peças de Arame  </a></h2>
          </li>-->

            <li>
              <a rel="nofollow" href="<?= $url ?>compressores-categoria" title="Compressores"><img src="imagens/compressores/compressores-01.jpg" alt="Compressores" title="Compressores" /></a>
              <h2><a href="<?= $url ?>compressores-categoria" title="Compressores">Compressores</a></h2>
            </li>

            <li>
              <a rel="nofollow" href="<?= $url ?>geradores-categoria" title="Geradores"><img src="imagens/geradores/geradores-01.jpg" alt="Geradores" title="Geradores" /></a>
              <h2><a href="<?= $url ?>geradores-categoria" title="Geradores">Geradores</a></h2>
            </li>

            <li>
              <a rel="nofollow" href="<?= $url ?>Rede-de-ar-pneumatica-categoria" title="Rede de ar pneumatica"><img src="imagens/geradores/conexao-pneumatica_11963_386686_1621359776317_cover.jpg" alt="Rede de ar pneumatica" title="Rede de ar pneumatica" /></a>
              <h2><a href="<?= $url ?>rede-de-ar-pneumatica-categoria" title="Rede de ar pneumatica">Rede de ar pneumatica</a></h2>
            </li>

            <li>
              <a rel="nofollow" href="<?= $url ?>Manutencao-de-compressor-categoria" title="Manutenção de compressor"><img src="imagens/geradores/manutencao-1.jpg" alt="Manutenção de compressor" title="Rede de ar pneumatica" /></a>
              <h2><a href="<?= $url ?>manutencao-de-compressor-categoria" title="Manutenção de compressor">Manutenção de compressor</a></h2>
            </li>

          </ul>
        </article>
      </div>
    </main>
    <? include('inc/form-mpi.php'); ?>
  </div>
  <? include('inc/footer.php'); ?>

</body>

</html>