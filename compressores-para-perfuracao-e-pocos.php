<? $h1 = "Compressores para Perfuração E Poços"; $title  = "Compressores para Perfuração E Poços"; $desc = "Faça uma cotação de $h1, você adquire no portal Soluções Industriais, solicite uma cotação imediatamente com aproximadamente 100 fornecedores"; $key  = "comprar Compressores para perfuração e poços,Compressores para perfuração e poços"; include('inc/head.php');  ?>
</head>

<body>
    <? include('inc/topo.php');?>
    <div class="wrapper">
        <main>
            <div class="content">
                <section><?=$caminhoinformacoes?><br class="clear" />
                    <h1><?=$h1?></h1>
                    <article>
                        <div class="img-mpi"><a href="<?=$url?>imagens/mpi/compressores-para-perfuracao-e-pocos-01.jpg"
                                title="<?=$h1?>" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/compressores-para-perfuracao-e-pocos-01.jpg"
                                    title="<?=$h1?>" alt="<?=$h1?>"></a><a
                                href="<?=$url?>imagens/mpi/compressores-para-perfuracao-e-pocos-02.jpg"
                                title="comprar Compressores para perfuração e poços" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/compressores-para-perfuracao-e-pocos-02.jpg"
                                    title="comprar Compressores para perfuração e poços"
                                    alt="comprar Compressores para perfuração e poços"></a><a
                                href="<?=$url?>imagens/mpi/compressores-para-perfuracao-e-pocos-03.jpg"
                                title="Compressores para perfuração e poços" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/compressores-para-perfuracao-e-pocos-03.jpg"
                                    title="Compressores para perfuração e poços"
                                    alt="Compressores para perfuração e poços"></a></div><span class="aviso">Estas
                            imagens foram obtidas de bancos de imagens públicas e disponível livremente na
                            internet</span>
                        <hr />
                        <h2>Sobre o compressor</h2>
                        <p>Os <strong>compressores para perfuração e poços</strong> possui itens de segurança que contam
                            com uma válvula de no tanque separador para alívio em caso de alta pressão e indicador de
                            saturação dos filtros de ar.</p>
                        <p>Possui maior área útil na carroceria devido suas dimensões reduzidas, o que proporciona maior
                            segurança para circulação do operador.</p>
                        <h2>Características em destaque</h2>
                        <ul>
                            <li class="li-mpi">Possui funções de segurança moderna;</li>
                            <li class="li-mpi">Desligamento automático em caso de altas temperaturas de óleo do
                                compressor ou caso ocorra baixo nível de óleo;</li>
                            <li class="li-mpi">Facilidade de manutenção e instalação;</li>
                            <li class="li-mpi">Economia de tempo e dinheiro;</li>
                            <li class="li-mpi">Excelência nas atividades em que o equipamento opera;</li>
                            <li class="li-mpi">Entre outros.</li>
                        </ul>
                        <p>Solicite agora mesmo o orçamento gratuito.</p>
                    </article>
                    <? include('inc/coluna-mpi.php');?><br class="clear">
                    <? include('inc/busca-mpi.php');?>
                    <? include('inc/form-mpi.php');?>
                    <? include('inc/regioes.php');?>
                </section>
            </div>
        </main>
    </div>
    <? include('inc/footer.php');?>
</body>

</html>