//iframe que receberá o video
    var empty_iframe_element = document.createElement('iframe');
    //id para estilizar
    empty_iframe_element.id = 'empty-iframe'

    //div que ativa a função
    var embedBox = document.getElementsByClassName('embed-box')
    //div na qual se faz o modal
    var lightbox_iframe = document.getElementById('lightbox-iframe')
    //div na qual se insere o iframe
    var iframe_field = document.getElementById('iframe-field')


    function show_empty_iframe(embed) {

        //captura o link do embed
        var this_embed = embedBox[embed].childNodes[1]
        var img_thumb_sliced_one = this_embed.src.slice(27)
        var split = img_thumb_sliced_one.split('/')
        var final_hash = split[0]

        //apend no iframe
        iframe_field.appendChild(empty_iframe_element)

        //transfere o link para o iframe vazio
        empty_iframe_element.src = "https://www.youtube.com/embed/" + final_hash

        //display flex no lightbox
        lightbox_iframe.style.display = 'flex'
    }

    function close_lightbox() {
        //display none no Lightbox e remoção do iframe
        lightbox_iframe.style.display = 'none'
        iframe_field.removeChild(empty_iframe_element)
    }