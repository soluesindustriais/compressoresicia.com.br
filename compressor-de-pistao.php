<? $h1 = "Compressor de Pistão"; $title  = "Compressor de Pistão"; $desc = "Orce $h1, você acha na maior vitrine Soluções Industriais, cote produtos hoje com dezenas de distribuidores ao mesmo tempo"; $key  = "comprar Compressor de pistão,Compressores de pistão"; include('inc/head.php');  ?>
</head>

<body>
    <? include('inc/topo.php');?>
    <div class="wrapper">
        <main>
            <div class="content">
                <section><?=$caminhoinformacoes?><br class="clear" />
                    <h1><?=$h1?></h1>
                    <article>
                        <div class="img-mpi"><a href="<?=$url?>imagens/mpi/compressor-de-pistao-01.jpg" title="<?=$h1?>"
                                class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/compressor-de-pistao-01.jpg"
                                    title="<?=$h1?>" alt="<?=$h1?>"></a><a
                                href="<?=$url?>imagens/mpi/compressor-de-pistao-02.jpg"
                                title="comprar Compressor de pistão" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/compressor-de-pistao-02.jpg"
                                    title="comprar Compressor de pistão" alt="comprar Compressor de pistão"></a><a
                                href="<?=$url?>imagens/mpi/compressor-de-pistao-03.jpg" title="Compressores de pistão"
                                class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/compressor-de-pistao-03.jpg"
                                    title="Compressores de pistão" alt="Compressores de pistão"></a></div><span
                            class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível
                            livremente na internet</span>
                        <hr />
                        <p>O <strong>compressor de pistão</strong> é um equipamento que consegue gerar ar comprimido e
                            impulsionar motores em processos industriais. Seu funcionamento se assemelha ao de um motor
                            de combustão interna de pistão, que atua sem a ignição por explosão e alimentação de
                            combustível.</p>
                        <p>O sistema de ar comprimido é fundamental para gerar potência em processos industriais, já que
                            é imprescindível contar com os compressores em qualquer linha de produção. Ferramentas
                            pneumáticas, instrumentações e máquinas dependem da operação do dispositivo para atuarem com
                            máximo desempenho.</p>
                        <h2>Como funciona</h2>
                        <p>O funcionamento do <strong>compressor de pistão</strong> ocorre por meio de pistões fechados
                            em cilindros, que geralmente possuem válvulas para entrada e saída de gases. Com isso, o ar
                            é compactado no interior do cilindro e expelido sob pressão, para que possa ser utilizado em
                            inúmeras aplicações industriais.</p>
                        <h2>Características técnicas:</h2>
                        <ul>
                            <li class="li-mpi">Potência de 50hp a 250hp</li>
                            <li class="li-mpi">Acoplamento de forma direta</li>
                            <li class="li-mpi">Possui secador e filtros integrados</li>
                            <li class="li-mpi">Inversor de frequência</li>
                            <li class="li-mpi">Sistema operacional feito via internet</li>
                            <li class="li-mpi">Unidades compressoras GHH / TMC</li>
                            <li class="li-mpi">Motores com alto rendimento.</li>
                        </ul>
                        <p>Solicite um orçamento e saiba onde comprar compressor de ar.</p>
                    </article>
                    <? include('inc/coluna-mpi.php');?><br class="clear">
                    <? include('inc/busca-mpi.php');?>
                    <? include('inc/form-mpi.php');?>
                    <? include('inc/regioes.php');?>
                </section>
            </div>
        </main>
    </div>
    <? include('inc/footer.php');?>
</body>

</html>