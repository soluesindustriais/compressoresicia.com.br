<?php
if (!$Read):
  $Read = new Read;
endif;
$arrBreadcrump = array();
if (isset($URL) && !in_array('', $URL)):
  $lastCategory = end($URL);
foreach ($URL as $paginas => $value):
  if (!empty($value)):
    $Read->ExeRead(TB_CATEGORIA, "WHERE cat_name = :nm AND cat_status = :st AND user_empresa = :emp", "nm={$value}&st=2&emp=" . EMPRESA_CLIENTE);
    if ($Read->getResult()):
      $itemSessao = $Read->getResult();
      $arrBreadcrump[] = array('titulo' => $itemSessao[0]['cat_title'], 'url' => $itemSessao[0]['cat_name'], 'parent' => $itemSessao[0]['cat_id']);
    endif;
    $Read->ExeRead(TB_CASE, "WHERE case_name = :nm AND user_empresa = :emp", "nm={$value}&emp=" . EMPRESA_CLIENTE);
    if ($Read->getResult()):
      $itemName = $Read->getResult();
      $arrBreadcrump[] = array('titulo' => $itemName[0]['case_title'], 'url' => $itemName[0]['case_name'], 'parent' => $itemName[0]['cat_parent']);
    endif;
  endif;
endforeach;
endif;
include('inc/head.php');
 ?>
<style><?include ('slick/slick.css');?></style>
</head>
<body>
  <?php include('inc/topo.php'); ?>
  <main>
    <div id="cases">
      <section>
        <div class="wrapper">
          <?php Check::SetBreadcrumb($arrBreadcrump); ?>
          <h1 class="breadcrumb-sig__title"><?php Check::SetTitulo($arrBreadcrump, $URL); ?> </h1>
          <?php
          $categ = Check::CatByName($lastCategory, EMPRESA_CLIENTE);
          if (!$categ): require 'inc/cases-inc.php';
          else:
            $Read->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_status = :stats AND cat_parent = :parent ORDER BY cat_date DESC", "emp=" . EMPRESA_CLIENTE . "&stats=2&parent={$categ}");
            if (!$Read->getResult()):
              $Read->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_status = :stats AND cat_id = :parent ORDER BY cat_date DESC", "emp=" . EMPRESA_CLIENTE . "&stats=2&parent={$categ}");
              if (!$Read->getResult()):
                WSErro("Desculpe, mas não foi encontrando o conteúdo relacionado a esta página, volte mais tarde", WS_INFOR, null, "Aviso!");
              else:
                $category = $Read->getResult();
                $category = $category[0]; ?>
                <!-- SE ESTIVER SETADO PARA TRUE NA CLIENT.INC.PHP MOSTRA DESCRIÇÃO DA CATEGORIA  -->
                <?php if (CAT_CONTENT): ?>
                  <div class="container">
                    <div class="category-content">
                      <?=$category['cat_content'];?>
                    </div>
                  </div>
                <? endif;
              endif; ?>
              <div class="container">
                <div class="grid grid-col-4">
                  <?php
                  $Read->ExeRead(TB_CASE, "WHERE user_empresa = :emp AND cat_parent = :cat AND case_status = :stats ORDER BY case_title ASC", "emp=" . EMPRESA_CLIENTE . "&cat={$categ}&stats=2");
                  if ($Read->getResult()):
                    foreach ($Read->getResult() as $cases):
                      extract($cases); ?>
                      <div class="case-card">
                        <a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $case_name; ?>" title="<?= $case_title; ?>">
                          <?= Check::Image('doutor/uploads/' . $case_cover, $case_title, 'case-card__cover', 212, 212) ?>
                          <div class="case-card__overlay"><h2 class="case-card__title"><?= $case_title; ?></h2></div>
                        </a>
                      </div>
                    <? endforeach;
                  endif; ?>
                </div>
              </div>
            <? else: ?>
              <div class="container">
                <div class="grid grid-col-4">
                  <?php
                  foreach ($Read->getResult() as $cat):
                    extract($cat); ?>
                    <div class="case-card">
                      <a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $cat_name; ?>" title="<?= $cat_title; ?>">
                        <?php
                        $Read->ExeRead(TB_CASE, "WHERE user_empresa = :emp AND case_status = :stats AND cat_parent = :parent ORDER BY case_title ASC LIMIT 1", "emp=" . EMPRESA_CLIENTE . "&stats=2&parent={$cat_id}");
                        if ($Read->getResult()):
                          foreach ($Read->getResult() as $cases):
                            echo Check::Image('doutor/uploads/' . $cases['case_cover'], $cat_title, 'case-card__cover', 212, 212);
                          endforeach;
                        else:
                          echo Check::Image('doutor/images/default.png', $cat_title, 'case-card__cover', 212, 212);
                        endif; ?>
                        <div class="case-card__overlay"><h2 class="case-card__title"><?= $case_title; ?></h2></div>
                      </a>
                    </div>
                    <?php
                  endforeach;
                  $Read->ExeRead(TB_CASE, "WHERE cat_parent = :cat AND user_empresa = :emp", "cat={$categ}&emp=" . EMPRESA_CLIENTE);
                  if ($Read->getResult()):
                    foreach ($Read->getResult() as $cases):
                      extract($cases); ?>
                      <div class="case-card">
                        <a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $case_name; ?>" title="<?= $case_title; ?>">
                          <?= Check::Image('doutor/uploads/' . $case_cover, $case_title, 'case-card__cover', 212, 212); ?>
                          <div class="case-card__overlay"><h2 class="case-card__title"><?= $case_title; ?></h2></div>
                        </a>
                      </div>
                    <? endforeach;
                  endif; ?>
                </div>
              </div>
            <? endif;
          endif; ?>
        </div> <!-- wrapper -->
        <div class="clear"></div>
      </section>
    </div> <!-- cases -->
  </main>
  <?php include('inc/footer.php'); ?>
</body>
</html>