<? $h1 = "Compressor Industrial"; $title  = "Compressor Industrial"; $desc = "Faça uma cotação de $h1, encontre as melhores indústrias, faça uma cotação imediatamente com aproximadamente 200 fornecedores ao mesmo tempo"; $key  = "comprar Compressor industrial,Compressores industriais"; include('inc/head.php');  ?>
</head>

<body>
    <? include('inc/topo.php');?>
    <div class="wrapper">
        <main>
            <div class="content">
                <section><?=$caminhoinformacoes?><br class="clear" />
                    <h1><?=$h1?></h1>
                    <article>
                        <div class="img-mpi"><a href="<?=$url?>imagens/mpi/compressor-industrial-01.jpg"
                                title="<?=$h1?>" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/compressor-industrial-01.jpg" title="<?=$h1?>"
                                    alt="<?=$h1?>"></a><a href="<?=$url?>imagens/mpi/compressor-industrial-02.jpg"
                                title="comprar Compressor industrial" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/compressor-industrial-02.jpg"
                                    title="comprar Compressor industrial" alt="comprar Compressor industrial"></a><a
                                href="<?=$url?>imagens/mpi/compressor-industrial-03.jpg"
                                title="Compressores industriais" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/compressor-industrial-03.jpg"
                                    title="Compressores industriais" alt="Compressores industriais"></a></div><span
                            class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível
                            livremente na internet</span>
                        <hr />
                        <h2>Sobre o equipamento</h2>
                        <p>Atualmente existem no mercado diversos tipos e modelos e compressores industriais. Existem
                            duas formas distintas de se obter ar comprimido, via dinâmica e via volumétrica. No formato
                            dinâmico estão os compressores de ar centrífugos e axiais. E no formato volumétrico estão os
                            compressores de ar alternativos e rotativos.</p>
                        <h2>Compressor para uso industrial</h2>
                        <p>Existem hoje diversos modelos com especificas funções de cada compressor, sendo que a maioria
                            desse é para o uso industrial. Os principais modelos de <strong>compressor
                                industrial</strong> são:</p>
                        <ul>
                            <li class="li-mpi">Compressores parafuso elétricos</li>
                            <li class="li-mpi">Compressores a diesel</li>
                            <li class="li-mpi">Compressores isentos de óleo</li>
                            <li class="li-mpi">Compressores de alta pressão - PET</li>
                            <li class="li-mpi">Compressores pistão silenciado</li>
                            <li class="li-mpi">Compressor scroll</li>
                            <li class="li-mpi">Compressores centrífugos</li>
                            <li class="li-mpi">Entre diversos outros.</li>
                        </ul>
                    </article>
                    <? include('inc/coluna-mpi.php');?><br class="clear">
                    <? include('inc/busca-mpi.php');?>
                    <? include('inc/form-mpi.php');?>
                    <? include('inc/regioes.php');?>
                </section>
            </div>
        </main>
    </div>
    <? include('inc/footer.php');?>
</body>

</html>