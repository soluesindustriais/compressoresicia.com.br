<?php

  $Update = new Update;
  $schedule = array('blog_status' => 2);
  $scheduleTimestamp = date("Y/m/d H:i:s", strtotime("2032-12-31 00:00:00"));
  $scheduleEndDate = array('blog_schedule' => $scheduleTimestamp);
  $Update->ExeUpdate(TB_BLOG, $schedule, "WHERE blog_schedule <= CURDATE() AND blog_status = :st", "st=1");
  if ($Update->getResult()):
  $Update->ExeUpdate(TB_BLOG, $scheduleEndDate, "WHERE blog_schedule <= CURDATE() AND blog_status = :st", "st=2");
  Check::UpdateSitemap("../sitemap.xml");
  endif;

if (!$Read):
  $Read = new Read;
endif;
$arrBreadcrump = array();
if (isset($URL) && !in_array('', $URL)):
  $lastCategory = end($URL);
foreach ($URL as $paginas => $value):
  if (!empty($value)):
    $Read->ExeRead(TB_CATEGORIA, "WHERE cat_name = :nm AND cat_status = :st", "nm={$value}&st=2");
    if ($Read->getResult()):
      $itemSessao = $Read->getResult();
      $arrBreadcrump[] = array('titulo' => $itemSessao[0]['cat_title'], 'url' => $itemSessao[0]['cat_name'], 'parent' => $itemSessao[0]['cat_id']);
      $itemParent = $itemSessao[0]['cat_parent'];
    endif;
    $Read->ExeRead(TB_BLOG, "WHERE blog_name = :nm", "nm={$value}");
    if ($Read->getResult()):
      $itemName = $Read->getResult();
      $arrBreadcrump[] = array('titulo' => $itemName[0]['blog_title'], 'url' => $itemName[0]['blog_name'], 'parent' => $itemName[0]['cat_parent']);
      $itemParent = $itemName[0]['cat_parent'];
    endif;
  endif;
endforeach;
endif;

$blogMonthList = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");

$Read->ExeRead(TB_CATEGORIA, "WHERE cat_status = :st", "st=2");
$categoryList = $Read->getResult();

include('inc/head.php');

include('inc/paginacao-blog-inc.php');
?>
<style>
  <?php include ('slick/slick.css');
    switch($itemSessao[0]['cat_theme']){
      case "Grid": include('css/blog-grid.css');
        break;
      case "List": include('css/blog-list.css');
          break;
      case "Full": include('css/blog-full.css');
          break;
      default:
        break;
    }
  ?>
</style>
</head>
<body>
  <?php include('inc/topo-blog.php'); ?>
  <main>
    <?php
    switch($itemSessao[0]['cat_theme']){
      case "Grid": include('doutor/layout/blog-grid/blog-grid.php');
        break;
      case "List": include('doutor/layout/blog-list/blog-list.php');
          break;
      case "Full": include('doutor/layout/blog-full/blog-full.php');
          break;
      default:
        include('doutor/layout/blog-default/blog-default.php'); 
    }
    ?>
  </main>
  <?php include('inc/footer-blog.php'); ?>
</body>
</html>