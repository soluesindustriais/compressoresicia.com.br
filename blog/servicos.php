<?php
if (!$Read):
  $Read = new Read;
endif;
$arrBreadcrump = array();
if (isset($URL) && !in_array('', $URL)):
  $lastCategory = end($URL);
foreach ($URL as $paginas => $value):
  if (!empty($value)):
    $Read->ExeRead(TB_CATEGORIA, "WHERE cat_name = :nm AND user_empresa = :emp", "nm={$value}&emp=" . EMPRESA_CLIENTE);
    if ($Read->getResult()):
      $itemSessao = $Read->getResult();
      $arrBreadcrump[] = array('titulo' => $itemSessao[0]['cat_title'], 'url' => $itemSessao[0]['cat_name'], 'parent' => $itemSessao[0]['cat_id']);
    endif;
    $Read->ExeRead(TB_SERVICO, "WHERE serv_name = :nm AND user_empresa = :emp", "nm={$value}&emp=" . EMPRESA_CLIENTE);
    if ($Read->getResult()):
      $itemName = $Read->getResult();
      $arrBreadcrump[] = array('titulo' => $itemName[0]['serv_title'], 'url' => $itemName[0]['serv_name'], 'parent' => $itemName[0]['cat_parent']);
    endif;
  endif;
endforeach;
endif;
include('inc/head.php');
 ?>
<style><?include ('slick/slick.css');?></style>
</head>
<body>
  <?php include('inc/topo.php'); ?>
  <main>
    <div id="servicos">
      <section itemscope itemtype="https://schema.org/Services">
       <div class="bread">
        <div class="wrapper">
          <div class="bread__row">
            <?php Check::SetBreadcrumb($arrBreadcrump); ?>
            <h1 class="bread__title"><?php Check::SetTitulo($arrBreadcrump, $URL); ?></h1>
          </div>
        </div>
      </div>
      <div class="wrapper">
        <?php
        $categ = Check::CatByName($lastCategory, EMPRESA_CLIENTE);
        if (!$categ): require 'inc/servico-inc.php';
          else: include('inc/paginacao-inc.php');
            $Read->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_status = :stats AND cat_parent = :parent ORDER BY cat_title ASC LIMIT :limit OFFSET :offset", "emp=" . EMPRESA_CLIENTE . "&stats=2&parent={$categ}&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
            if (!$Read->getResult()):
              $Read->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_status = :stats AND cat_id = :parent ORDER BY cat_title ASC LIMIT :limit OFFSET :offset", "emp=" . EMPRESA_CLIENTE . "&stats=2&parent={$categ}&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
              if ($Read->getResult()):
                $category = $category[0];
                foreach ($Read->getResult() as $cat):
                  extract($cat); ?>
                  <!-- SE ESTIVER SETADO PARA TRUE NA CLIENT.INC.PHP MOSTRA DESCRIÇÃO DA CATEGORIA  -->
                  <?php if (CAT_CONTENT): ?>
                    <div class="container">
                      <div class="category-content">
                        <?=$cat_content?>
                      </div>
                    </div>
                  <? endif;
                endforeach;
              endif; ?>
              <div class="container">
                <div class="grid grid-col-4">
                  <?php
                  $Read->ExeRead(TB_SERVICO, "WHERE user_empresa = :emp AND FIND_IN_SET(:cat, cat_parent) AND serv_status = :stats ORDER BY serv_title ASC LIMIT :limit OFFSET :offset", "emp=" . EMPRESA_CLIENTE . "&cat={$categ}&stats=2&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
                  if ($Read->getResult()):
                    foreach ($Read->getResult() as $prod):
                      extract($prod); ?>
                      <div class="card card--prod">
                        <a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $serv_name; ?>" title="<?= $serv_title; ?>">
                          <img class="card__cover" src="<?=RAIZ?>/doutor/uploads/<?=$serv_cover?>" alt="<?=$serv_title?>" title="<?=$serv_title?>">
                          <h2 class="card__title"><?= $serv_title; ?></h2>
                        </a>
                      </div>
                    <? endforeach;
                  endif; ?>
                </div>
              </div>
              <!-- PAGINAÇÃO -->
              <div class="float-right">
                <?php
                $Pager->ExePaginator(TB_SERVICO, "WHERE user_empresa = :emp AND FIND_IN_SET(:cat, cat_parent) AND serv_status = :stats", "emp=" . EMPRESA_CLIENTE . "&cat={$categ}&stats=2");
                echo $Pager->getPaginator(); ?>
              </div>
              <?php
            else:
              $lerCat = new Read;
              $lerCat->ExeRead(TB_CATEGORIA, "WHERE cat_id = :id AND user_empresa = :emp", "id={$Read->getResult()[0]['cat_parent']}&emp=".EMPRESA_CLIENTE);
              if ($lerCat->getResult()):
                $categPai = $lerCat->getResult()[0]; ?>
                <!-- SE ESTIVER SETADO PARA TRUE NA CLIENT.INC.PHP MOSTRA DESCRIÇÃO DA CATEGORIA  -->
                <?php if (CAT_CONTENT): ?>
                  <div class="category-content">
                    <?= $categPai['cat_content']; ?>
                  </div>
                <?php endif;
              endif; ?>
              <div class="container">
                <div class="grid grid-col-4">
                  <?php
                  foreach ($Read->getResult() as $cat):
                    extract($cat); ?>
                    <div class="card card--sprod>
                      <a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $cat_name; ?>" title="<?= $cat_title; ?>">
                        <?php
                        if (empty($cat_cover)):
                          $Read->ExeRead(TB_SERVICO, "WHERE user_empresa = :emp AND serv_status = :stats AND FIND_IN_SET(:cat, cat_parent) ORDER BY serv_date DESC LIMIT 1", "emp=" . EMPRESA_CLIENTE . "&stats=2&cat={$cat_id}");
                          if ($Read->getResult()):
                            foreach ($Read->getResult() as $prod):?>
                              <img class="card__cover" src="<?=RAIZ?>/doutor/uploads/<?=$serv_cover?>" alt="<?=$cat_title?>" title="<?=$cat_title?>">
                              <?
                            endforeach;
                          else: ?>
                            <img class="card__cover" src="<?=RAIZ?>/doutor/images/default.png" alt="<?=$cat_title?>" title="<?=$cat_title?>">
                            <?
                          endif;
                        else: ?>
                          <img class="card__cover" src="<?=RAIZ?>/doutor/uploads/<?=$cat_cover?>" alt="<?=$cat_title?>" title="<?=$cat_title?>">
                        endif; ?>
                        <h2 class="card__title"><?= $cat_title; ?></h2>
                      </a>
                    </div>
                  <? endforeach;
                  $Read->ExeRead(TB_SERVICO, "WHERE FIND_IN_SET(:cat, cat_parent) AND serv_status = :st AND user_empresa = :emp ORDER BY serv_title ASC LIMIT :limit OFFSET :offset", "cat={$categ}&st=2&emp=" . EMPRESA_CLIENTE . "&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
                  if ($Read->getResult()):
                    foreach ($Read->getResult() as $produto):
                      extract($produto); ?>
                      <div class="card card--sprod>
                        <a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $serv_name; ?>" title="<?= $serv_title; ?>">
                          <img class="card__cover" src="<?=RAIZ?>/doutor/uploads/<?=$serv_cover?>" alt="<?=$serv_title?>" title="<?=$serv_title?>">
                          <h2 class="card__title"><?= $serv_title; ?></h2>
                        </a>
                      </div>
                    <? endforeach;
                  endif; ?>
                </div>
              </div>
              <!-- PAGINAÇÃO -->
              <div class="float-right">
                <?php
                $Pager->ExePaginator(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_status = :stats AND cat_parent = :parent ORDER BY cat_title ASC", "emp=" . EMPRESA_CLIENTE . "&stats=2&parent={$categ}");
                echo $Pager->getPaginator(); ?>
              </div>
            <? endif;
          endif; ?>
        </div> <!-- wrapper -->
        <div class="clear"></div>
      </section>
    </div> <!-- servicos -->
  </main>
  <?php include('inc/footer.php'); ?>
</body>
</html>