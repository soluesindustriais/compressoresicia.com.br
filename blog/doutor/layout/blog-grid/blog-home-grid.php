<div class="blog-home blog-grid">
	<div class="container">
		<div class="wrapper">
			<h2 class="text-center dark">Últimas postagens</h2>
			<div class="row justify-content-center gap-30 pt-5">
				<?php
				if ($Read->getResult()) :
					foreach ($Read->getResult() as $dados) :
						extract($dados); ?>
						<div class="blog-card col-4">
							<div class="blog-card__image">
								<a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog_name; ?>" title="<?= $blog_title; ?>">
									<img class="blog-card__cover" src="<?= RAIZ ?>/doutor/uploads/<?= $blog_cover ?>" alt="<?= $blog_title ?>" title="<?= $blog_title ?>">
								</a>

								<?php
									$blogCatUrl = Check::CatByParent($cat_parent, EMPRESA_CLIENTE);
									$blogCatUrlFinal = explode("/", $blogCatUrl); 
									array_pop($blogCatUrlFinal);                       
									$blogCatTitle = Check::CatByUrl(end($blogCatUrlFinal), EMPRESA_CLIENTE);
									$blogCatUrl = RAIZ . "/". substr($blogCatUrl, 0, -1);
								?>
								<a class="blog-card__category" href="<?=$blogCatUrl?>" title="<?=$blogCatTitle?>"><?=$blogCatTitle?></a>
							</div>

							<div class="blog-card__info">
								<h3 class="blog-card__title"><a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog_name; ?>" title="<?= $blog_title; ?>"><?= $blog_title ?></a></h3>
								<?php $newDate = explode("/", date("d/m/Y", strtotime($blog_date)));
								$blogDay = $newDate[0];
								$blogMonth = $newDate[1];
								$blogYear = $newDate[2];
								$blogFullDate = $blogDay . " de " . $blogMonthList[$blogMonth - 1] . " de " . $blogYear;
								?>
								<p class="blog-card__date"><?= $blogFullDate ?></p>

								<div class="blog-card__author">
									<?php 
										$authorKey = array_search($user_id, array_column($authors, 'user_id'));
										$authorName = $authors[$authorKey]['user_name'];
									?>
									Por:
									<a href="<?=$url?>autor/<?= urlencode($authorName) ?>" rel="nofollow" title="<?= $authorName ?>"><?= $authorName ?></a>
								</div>

								<div class="blog-card__description">
									<?php if (BLOG_BREVEDESC && isset($blog_brevedescription)) : ?>
										<p class="blog-card__content-text"><?= $blog_brevedescription ?></p>
									<?php else : ?>
										<p class="blog-card__content-text"><?= Check::Words($blog_content, 25); ?></p>
									<?php endif; ?>
								</div>
							</div>
						</div>
				<? endforeach;
				endif; ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>