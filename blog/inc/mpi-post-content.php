<? include('inc/coluna-lateral-icm.php'); ?>
<div class="clear"></div>
<hr>
<div class="row mpi-cta">
    <div class="col-8">
        <h2 class="fs-28 mb-2">Encontrou o que procurava?</h2>
        <p class="fs-18 text-left">Faça seu orçamento grátis agora mesmo!</p>
    </div>
    <div class="col-4 d-flex align-items-center justify-content-center">
        <a href="javascript:;" title="Quero meu orçamento" class="btn lightbox" src="#modal-form-contato">Quero meu orçamento</a>
    </div>
</div>
<hr>
<? include('inc/paginas-relacionadas-icm.php'); ?>
<hr>
<? include('inc/regioes-brasil.php'); ?>
<br class="clear">
<? include('inc/copyright-icm.php'); ?>