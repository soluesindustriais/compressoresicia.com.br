<div class="grid">
    <div class="col-6">
        <div class="picture-legend picture-center"><a href="<?= $url ?>imagens/compressores/compressores-01.jpg" class="lightbox" title="<?= $h1 ?>" target="_blank"><img class="lazyload" src="<?= $url ?>imagens/compressores/thumbs/compressores-01.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>" /></a><strong>Imagem ilustrativa de <?= $h1 ?></strong></div>
    </div>
    <div class="col-6">
        <div class="picture-legend picture-center"><a href="<?= $url ?>imagens/compressores/compressores-02.jpg" class="lightbox" title="<?= $h1 ?>" target="_blank"><img class="lazyload" src="<?= $url ?>imagens/compressores/thumbs/compressores-02.jpg" alt="<?= $h1 ?>" title="<?= $h1 ?>" /></a><strong>Imagem ilustrativa de <?= $h1 ?></strong></div>
    </div>
</div>

