<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>

<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close">&times;</span>

        <form action="https://formsubmit.co/thomas.rodrigues@idealtrends.com.br" method="post">

            <h2 id="modal-title">Cote grátis agora!</h2>
            <p>Além de receber um orçamento, você também poderá esclarecer suas dúvidas referentes ao assunto. </p>
            <b id="campo-obrigatorio">* Campo obrigatório</b>
            <br>

            <div class="divider-field">
                <hr class="divider">
            </div>

            <section class="input-fields">

                <input type="text" name="nome" placeholder="Nome *" required>
                <input type="text" id="email-modal" name="email" placeholder="Email *" required>

                <input type="text" id="cnpj-modal" name="cnpj" placeholder="CNPJ *" required>
                <div class="dual-input-fields">
                    <div class="dual-input">
                        <input type="text" id="fone-modal" name="telefone" placeholder="telefone *" required>
                        <input type="text" id="cell-modal" name="celular" placeholder="celular *" required>
                    </div>
                </div>

                <textarea name="mensagem" cols="30" rows="4" placeholder="Mensagem *" required></textarea>

                <section class="btn-field">
                    <button type="submit" id="btn-modal" class="btn btn-primary btn-block font-weight-bold py-3 d-flex justify-content-center align-items-center btnSubmit">
                        Enviar Orçamento
                    </button>
                </section>

                <section class="">
                    <button id="btn-modal-exit" class="btn-fechar-modal">
                        Fechar
                    </button>
                </section>

                <input type="hidden" name="_cc" value="kaue.dias@idealtrends.com.br">

                <!-- Evitar Spam -->
                <input type="text" name="_honey" style="display:none">

                <!-- Capturando tamanho da tela -->
                <input type="hidden" id="screen-size-two" name="tamanho da tela" value="">

                <!-- Capturando página origem da mensagem -->
                <input type="hidden" id="lead-origin-two" name="página de origem">

                <!-- Capurar categoria -->
                <input type="hidden" id="categoria" name="categoria" value="Compressores">

                <!-- Página destino após forumulário ser preenchido -->
                <input id="next-page-two" type="hidden" name="_next" value="">
                <input type="hidden" name="_captcha" value="false">
                <input type="hidden" name="_autoresponse" value="Obrigado por enviar seu Email para o Soulções Industriais, logo responderemos sua mensagem!, Obrigado pelo contato!">


            </section>

        </form>

    </div>

</div>